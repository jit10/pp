#ifndef COMMON_H
#define COMMON_H

#include "i.h"

#define STRINGIFY(x) #x
#define UNQ_NAME(s) STRINGIFY(UnIQuERtTrAcEPrOf_) #s

#define UNQ_VAR(x) UnIQuERtTrAcEPrOf_ ## x

#define ROUNDUP(x, y) ((((x) + ((y) - 1)) / (y)) * (y))

#define VAL_STREAM "val_fifo"
#define DDEP_STREAM "ddep_fifo"
#define CDEP_STREAM "cdep_fifo"
#define PATH_STREAM "path_fifo"
#define CTRL_STREAM "ctrl_fifo"

// y is atmost 30bits.
#define mkctrl_sym(x, y) (((u32)(x)<<30) + (y))

#define NTRACE 4

enum TraceType : u8 {
	VAL = 0,
	DDEP = 1,
	CDEP = 2,
	PATH = 3,
};


// NOTE: y is atmost 61bits.
#define mk_trace_sym(x, y) ((x) + ((u64)(y)<<61))
#define is_sym_of(x, y) ((x)>>61 == (u64)(y))

enum struct TraceSym : u8 {
	E = 0,
	P = 1,
	L = 2,
	S = 3,
	J = 4,
	I = 5,
	C = 6,
};

#endif
