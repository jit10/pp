
namespace llvm {

template <> struct GraphTraits<libepid::CallGraph*> {
  using NodeType       = libepid::CGFunction ;
  using SiteIterator   = libepid::CGFunction::SiteIterator;
  using TargetIterator = libepid::CGSite::EdgeIterator;

  struct ChildIteratorType
      : public std::iterator<std::input_iterator_tag, int> {
    SiteIterator siteIt;
    SiteIterator siteEnd;
    TargetIterator targetIt;
    ChildIteratorType(SiteIterator it, SiteIterator end)
      : siteIt(it),
        siteEnd(end),
        targetIt() {
      if (siteIt != siteEnd) {
        targetIt = siteIt->edges.begin();
      }
    }
    ChildIteratorType(ChildIteratorType const &other)
      : siteIt(other.siteIt),
        siteEnd(other.siteEnd),
        targetIt(other.targetIt)
        { }

    ChildIteratorType& operator++() {
      assert(siteIt != siteEnd
        && "Incrementing past the end of callgraph iterator");
      ++targetIt;
      if (targetIt == siteIt->edges.end()) {
        ++siteIt;
        if (siteIt != siteEnd) {
          targetIt = siteIt->edges.begin();
        }
      }
      return *this;
    }
    ChildIteratorType operator++(int) {
      ChildIteratorType result(*this);
      ++*this;
      return result;
    }
    bool operator==(ChildIteratorType const & rhs) const {
      if (siteIt != rhs.siteIt) {
        return false;
      }
      return siteIt == siteEnd || targetIt == rhs.targetIt;
    }
    bool operator!=(ChildIteratorType const & rhs) const {
      return !(*this == rhs);
    }
    libepid::CGFunction const * operator*() const {
      assert(siteIt != siteEnd && "Dereferencing past the end of list");
      return targetIt->target;
    }
    libepid::CGEdge const & asEdge() const {
      assert(siteIt != siteEnd && "Dereferencing past the end of list");
      return *targetIt;
    }
  };


  static NodeType *getEntryNode(libepid::CallGraph *CG) {
    return &CG->functions[0];
  }
  static inline ChildIteratorType child_begin(NodeType *N) {
    return ChildIteratorType{ N->callsites.begin(), N->callsites.end() };
  }
  static inline ChildIteratorType child_end(NodeType *N) {
    return ChildIteratorType{  N->callsites.end(), N->callsites.end() };
  }

  using nodes_iterator = std::vector<libepid::CGFunction>::iterator;
  static nodes_iterator nodes_begin(libepid::CallGraph *CG) {
    return CG->functions.begin();
  }
  static nodes_iterator nodes_end  (libepid::CallGraph *CG){
    return CG->functions.end();
  }
  static unsigned size (libepid::CallGraph const *CG) {
    return CG->functions.size();
  }
};


template<>
struct DOTGraphTraits<libepid::CallGraph*>
  : public DefaultDOTGraphTraits {

  explicit DOTGraphTraits(bool isSimple=false)
    : DefaultDOTGraphTraits(isSimple)
      { }

  static std::string getGraphName(libepid::CallGraph const *CG) {
    std::string name("Approximate call graph");
    if (CG->functions.size() > 1) {
      Module *m = CG->functions[1].fun->getParent();
      name += " for " + m->getModuleIdentifier();
    }
    return name;
  }

  static std::string getSimpleNodeLabel(libepid::CGFunction const *Node,
                                        libepid::CallGraph const *CG) {
    return Node->getName();
  }

  static std::string getCompleteNodeLabel(libepid::CGFunction const *Node, 
                                          libepid::CallGraph const *CG) {
    std::string Str(getSimpleNodeLabel(Node, CG));
    raw_string_ostream OS(Str);

    OS << "\n";
    if (Node->fun) {
      Node->fun->getFunctionType()->print(OS);
      OS << "\n" << (!Node->fun->isVarArg() ? "non ":"") << "vararg\n";
    }

    return OS.str();
  }

  std::string getNodeLabel(libepid::CGFunction const *Node,
                           libepid::CallGraph const *Graph) {
    if (isSimple()) {
      return getSimpleNodeLabel(Node, Graph);
    } else {
      return getCompleteNodeLabel(Node, Graph);
    }
  }

  using EdgeIter = GraphTraits<libepid::CallGraph*>::ChildIteratorType;
  static std::string getEdgeAttributes(void const *, EdgeIter it,
                                       libepid::CallGraph const *CG) {
    return (it.asEdge().isBackedge) ? "style=dotted" : "style=solid";
  }
};


}


