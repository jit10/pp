#ifndef SUFFIX_H
#define SUFFIX_H

#include <array>
#include <cassert>
#include <functional>
#include <type_traits>
#include <unordered_map>
#include <vector>

namespace suffixarray {

/* Resources :
 * "Two Efficient Algorithms for Linear Time Suffix Array Construction" (Induced sorting)
 * http://zork.net/~st/jottings/sais.html -- walkthrough with example
 * "Linear-Time Longest-Common-Prefix Computation in Suffix Arrays and Its Applications" : LCP (GetHeight)
 * TODO: "Inducing LCP array" - extend the induced sorting algo to get LCP simultaneously.
 * "Suffix Arrays: A New Method for On-Line String Searches" : Search (super accelerant implemented)
 */

namespace detail {
/* copied from io.h to remove dependency. */
template<typename, typename, typename = void>
struct is_iterator_of : std::false_type {};

template<typename It, typename T>
struct is_iterator_of<It, T,
	typename std::enable_if_t<std::is_same<typename std::remove_reference<decltype(*std::declval<It>())>::type, T>::value || std::is_assignable<decltype(*std::declval<It>()), decltype(std::declval<T>())>::value>>
	: std::true_type {};
/* */

template<typename, typename = void>
struct is_int : std::false_type {};

template<typename T>
struct is_int<T, typename std::enable_if_t<std::is_integral<T>::value && !std::is_same<T, bool>::value && (sizeof(T) <= 64)>> : std::true_type {};

// LSD-2^8 radix sort.(returns offsets of sorted elems)
template<typename It, typename T = typename std::decay<decltype(*std::declval<It>())>::type>
std::vector<size_t>
radixSort(It begin, It end)
{
	static_assert(detail::is_int<T>::value, "Iterator must point to integral type with max width of 64bits");

	bool is_uint = std::is_unsigned<T>::value;

	size_t n = end-begin;
	std::vector<size_t> v, v1(n);
	if (n == 0) {
		return v;
	}

	v.reserve(n);
	for (size_t i = 0; i < n; i++) {
		v.push_back(i);
	}

	size_t sz = 8*sizeof(T);
	bool last = false;
	for (size_t n = 0; n < sz; n += 8) {
		if (n+8 >= sz) {
			last = true;
		}
		std::array<size_t, 256> counts{};
		std::array<decltype(v.begin()), 256> bucketHeads{};
		for (auto i : v) {
			if (!is_uint && last) {
				counts[static_cast<u8>((*(begin+i) >> n) ^ 0x80U)]++;
			} else {
				counts[static_cast<u8>(*(begin+i) >> n)]++;
			}
		}
		{
			auto v1_it = v1.begin();
			for (auto i = 0; i < 256; v1_it += counts[i++]) {
				bucketHeads[i] = v1_it;
			}
		}
		for (auto i : v) {
			if (!is_uint && last) {
				*bucketHeads[static_cast<u8>((*(begin+i) >> n) ^ 0x80U)]++ = i;
			} else {
				*bucketHeads[static_cast<u8>(*(begin+i) >> n)]++ = i;
			}
		}
		v.swap(v1);
	}

	return v;
}

// true => original string (whose alphabet is unknown and need to radix sorted to give lexicographical values)
// false => in recursion (0 < alphabet < n)
template<typename It, bool> struct _suffixBase;

template<typename It>
struct _suffixBase<It, true> {
	using T = typename std::decay<decltype(*std::declval<It>())>::type;
protected:
	size_t n;
	It begin, end;
	std::vector<T> v;

	_suffixBase(It begin, It end)
	:n(end-begin), begin(begin), end(end), v(n)
	{
		auto sn = radixSort(begin, end);
		size_t k = 0;
		v[sn[0]] = k;
		for (size_t i = 1; i < n; i++) {
			if (*(begin+sn[i]) != *(begin+sn[i-1])) {
				k++;
			}
			v[sn[i]] = k;
		}
	}

	decltype(auto)
	getBucketInfo(bool b)
	{
		std::vector<size_t> c(n, 0), m(n); // T -> true => bucketHeads/ false => bucketTails

		for (auto i : v) {
			c[i]++;
		}

		size_t k = 0;
		if (b) {
			for (size_t i = 0; i < n; i++) {
				m[i] = k;
				k += c[i];
			}
		} else {
			for (size_t i = 0; i < n; i++) {
				k += c[i];
				m[i] = k - 1;
			}
		}

		return m;
	}

	size_t
	bcktIdx(size_t i)
	{
		return v[i];
	}
};

template<typename It>
struct _suffixBase<It, false> {
	using T = typename std::decay<decltype(*std::declval<It>())>::type;
protected:
	size_t n;
	It begin, end;

	_suffixBase(It begin, It end) : n(end-begin), begin(begin), end(end) {}

	decltype(auto)
	getBucketInfo(bool b)
	{
		std::vector<size_t> c(n, 0), m(n);
		for (auto it = begin; it != end; it++) {
			c[*it]++;
		}

		size_t k = 0;
		if (b) {
			for (size_t i = 0; i < n; i++) {
				m[i] = k;
				k += c[i];
			}
		} else {
			for (size_t i = 0; i < n; i++) {
				k += c[i];
				m[i] = k - 1;
			}
		}

		return m;
	}

	size_t
	bcktIdx(size_t i)
	{
		return *(begin+i);
	}
};

template<typename It, bool is_top>
struct suffixSort : _suffixBase<It, is_top> {
	using _suffixBase<It, is_top>::getBucketInfo;
	using _suffixBase<It, is_top>::bcktIdx;
	using _suffixBase<It, is_top>::n;
	using _suffixBase<It, is_top>::begin;
	using _suffixBase<It, is_top>::end;

private:
	using T = typename std::decay<decltype(*std::declval<It>())>::type;
	enum suf_t : bool {L = false, S = true};
	std::vector<bool> suf_types;
	std::vector<size_t>& sa;

	bool
	is_lms(size_t i)
	{
		// assert(i < n && "out of bound");
		if (i > 0 && suf_types[i] == suf_t::S && suf_types[i-1] == suf_t::L) {
			return true;
		}
		return false;
	}

	bool is_lms_substr_eq(size_t i, size_t j)
	{
		size_t d = 0, k = std::max(i, j);
		for (; d < n-k; d++) {
			if (*(begin+i+d) != *(begin+j+d) || suf_types[i+d] != suf_types[j+d]) {
				return false;
			} else if (d > 0 && (is_lms(i+d) || is_lms(j+d))) {
				break;
			}
		}
		if (d == n-k) {
			if (is_lms(std::min(i, j)+d)) {
				return true;
			}
		} else if (is_lms(i+d) && is_lms(j+d)) {
			return true;
		}
		return false;
	}

	void
	induceL()
	{
		auto bucket_info = getBucketInfo(true);
		// last elem = L-type.(corresponding to the empty suffix LMS).
		auto& b = bucket_info[bcktIdx(n-1)];
		sa[b++] = n-1;
		for (size_t i = 0; i < n; i++) {
			auto e = sa[i];
			if (e == size_t(-1)) {
				continue;
			}
			if (e > 0 && suf_types[e-1] == suf_t::L) {
				auto& b = bucket_info[bcktIdx(e-1)];
				sa[b++] = e-1;
			}
		}
	}

	void
	induceS()
	{
		auto bucket_info = getBucketInfo(false);
		for (s64 i = n-1; i >= 0; i--) {
			auto e = sa[i];
			if (e == size_t(-1)) {
				continue;
			}
			assert(e < n && "problem");
			if (e > 0 && suf_types[e-1] == suf_t::S) {
				auto& b = bucket_info[bcktIdx(e-1)];
				sa[b--] = e-1;
			}
		}
	}

public:
	suffixSort(It begin, It end, std::vector<size_t>& sa)
	: _suffixBase<It, is_top>(begin, end), suf_types(n), sa(sa)
	{
		assert(s64(end-begin) > 0 && "invariant begin < end violated");
		assert(sa.size() >= size_t(end-begin) && "suffix array too small");
	}

	void
	run()
	{
		// stage1
		std::fill(sa.begin(), sa.begin()+n, size_t(-1));

//		std::cout << "entering length " << n << "\n";
		suf_types[n-1] = suf_t::L;
		for (s64 i = n-2; i >= 0; i--) {
			auto cur = begin+i;
			if (*cur > *(cur+1) || (*cur == *(cur+1) && suf_types[i+1] == suf_t::L)) {
				suf_types[i] = suf_t::L;
			} else {
				suf_types[i] = suf_t::S;
			}
		}

		{ // new block scope to force destructor of bucket_info.
			auto bucket_info = getBucketInfo(false);
			for (auto it = begin; it != end; it++) {
				if (is_lms(it - begin)) {
					auto &b = bucket_info[bcktIdx(it-begin)];
					sa[b--] = it - begin;
				}
			}
		}

		induceL();
		induceS();

		size_t m = 0;
		for(size_t i = 0; i < n; i++) {
			if (is_lms(sa[i])) {
				sa[m++] = sa[i];
			}
		}
		// post cond: 2*m <= n

		std::fill(sa.begin()+m, sa.begin()+n, size_t(-1));

		// summary string.
		size_t name = 0, prev = size_t(-1);
		for (size_t i = 0; i < m; i++) {
			auto pos = sa[i];
			if(prev == size_t(-1) || !is_lms_substr_eq(prev, pos)) { // distinct lms substr => new name
				name++;
				prev = pos;
			}
			pos = (pos%2)? (pos-1)/2: pos/2;
			sa[m+pos] = name-1;	// summary string(no need to allocate extra at each recursion)
		}
		// move summary string to end of sa.
		for(s64 i = n-1, j = n-1; i >= s64(m); i--) {
			if(sa[i] != size_t(-1)) {
				sa[j--] = sa[i];
			}
		}

		// stage2 : recurse if duplicates.
		auto sumIt = sa.begin()+n-m;
		if (name < m) {
			suffixSort<decltype(sumIt), false>(sumIt, sumIt+m, sa).run();
		} else {
			for (size_t i = 0; i < m; i++) {
				sa[*(sumIt+i)] = i;
			}
		}

		// stage3
		if (m > 0) {
			for (size_t i = 0, j = 0; i < n; i++) {
				if (is_lms(i)) {
					*(sumIt+j++) = i;
				}
			}
			for (size_t i = 0; i < m; i++) {
				sa[i] = *(sumIt+sa[i]);
			}

			// accurate LMS suffix bucket sort.
			auto bucket_info = getBucketInfo(false);
			std::fill(sa.begin()+m, sa.begin()+n, size_t(-1));
			for (s64 i = m-1; i >= 0; i--) {
				auto j = sa[i];
				auto& b = bucket_info[bcktIdx(j)];
				sa[i] = size_t(-1);
				sa[b--] = j;
			}
		}

		induceL();
		induceS();
	}
};

// Manbers Myers suffix sort.(way slower than induced sorting)
template<typename It>
struct MMsuffixSort {
	size_t n;
	It begin, end;
	std::vector<size_t>& sa;
	std::vector<std::pair<size_t, size_t>> rank;

public:
	MMsuffixSort(It begin, It end, std::vector<size_t>& sa)
	:n(end-begin), begin(begin), end(end), sa(sa)
	{
		assert(s64(end-begin) > 0 && "invariant begin < end violated");
		assert(sa.size() >= size_t(end-begin) && "suffix array too small");
	}


	void
	rankSort(std::vector<size_t>& sa)
	{
		std::vector<size_t> v1(n);
		std::vector<size_t> counts(n+1), bucketHeads(n+1);
		size_t k = 0;

		// bucket sort based on LSD.
		for (auto i : sa) {
			counts[rank[i].second+1]++;
		}
		for (size_t i = 0; i < n+1; k += counts[i++]) {
			bucketHeads[i] = k;
		}
		for (auto i : sa) {
			v1[bucketHeads[rank[i].second+1]++] = i;
		}

		// bucket sort based on MSD.
		k = 0;
		std::fill(counts.begin(), counts.end(), 0);
		for (auto i : v1) {
			counts[rank[i].first+1]++;
		}
		for (size_t i = 0; i < n+1; k += counts[i++]) {
			bucketHeads[i] = k;
		}
		for (auto i : v1) {
			sa[bucketHeads[rank[i].first+1]++] = i;
		}
	}

	void
	run()
	{
		auto sn = radixSort(begin, end);
		std::vector<s64> names(n);
		s64 k = 0;
		names[sn[0]] = k;
		for (size_t i = 1; i < n; i++) {
			if (*(begin+sn[i]) != *(begin+sn[i-1])) {
				k++;
			}
			names[sn[i]] = k;
		}

		rank.reserve(n);
		for (size_t i = 0; i < n-1; i++) {
			sa[i] = i;
			rank.push_back(std::make_pair(names[i], names[i+1]));
		}
		sa[n-1] = n-1;
		rank.push_back(std::make_pair(names[n-1], s64(-1)));

/*		auto cmp_fn = [&](size_t i, size_t j) {
			return rank[i] < rank[j];
		};*/

		rankSort(sa);
		//std::sort(sa.begin(), sa.end(), cmp_fn);

		// lgn steps.
		for (size_t h = 4; h < 2*n; h *= 2) {
			size_t k = 0;
			// rank MSD based on bucket.
			auto prev_rank = rank[sa[0]];
			rank[sa[0]].first = 0;
			for (size_t i = 1; i < n; i++) {
				if (rank[sa[i]] != prev_rank) {
					k++;
				}

				prev_rank = rank[sa[i]];
				rank[sa[i]].first = k;
			}

			for (size_t i = 0; i < n; i++) {
				rank[sa[i]].second = sa[i]+h/2 < n ? rank[sa[i]+h/2].first : s64(-1);
			}

			rankSort(sa);
			//std::sort(sa.begin(), sa.end(), cmp_fn);
		}
	}
};
}

// TODO: can change if 0 to compiler flag for super accelerant.
template<typename It>
struct Dictionary {
	using T = typename std::decay<decltype(*std::declval<It>())>::type;
private:
	size_t n;
	It begin, end;
	std::vector<size_t> sa;
#if 0
	std::vector<size_t> llcp, rlcp;
#endif

	decltype(auto)
	getLCP()
	{
		std::vector<size_t> lcp(n), inv(n);

		for (size_t i = 0; i < n; i++) {
			inv[sa[i]] = i;
		}

		size_t h = 0;
		for (size_t i = 0; i < n; i++) {
			auto x = inv[i];
			if (x == 0) {
				lcp[x] = 0;
				continue;
			}
			auto j = sa[x-1];
			auto it1 = begin + i + h;
			auto it2 = begin + j + h;
			while(it1 != end && it2 != end && *it1++ == *it2++) {
				h++;
			}
			lcp[x] = h;
			if (h > 0) {
				h--;
			}
		}

		return lcp;
	}

	void
	getLRLCP()
	{
		// lcp(x) = lcp_fn(begin+sa[x], begin+sa[x-1])
		auto lcp = getLCP();

		// llcp and rlcp have size of n-2 (n-2 nodes in binary search tree)
		std::function<size_t(size_t, size_t)> helper =
		[&](size_t l, size_t r) {
			if (l == r-1) {
				return lcp[r];
			}
			auto c = (l+r)/2;
			llcp[c-1] = helper(l, c);
			rlcp[c-1] = helper(c, r);
			return std::min(llcp[c-1], rlcp[c-1]);
		};

		helper(0, n-1);
	}

public:
	Dictionary(It begin, It end)
	:n(end-begin), begin(begin), end(end), sa(n)
	{
		assert(s64(end-begin) >= 0 && "dictionary constructor begin <= end violated");
		if (n <= 1) {
			return;
		}

#if 0
		llcp = std::vector<size_t>(n-2);
		rlcp = std::vector<size_t>(n-2);
#endif
		assert(end-begin >= 0 && "invariant begin <= end violated");
		detail::suffixSort<It, true>(begin, end, sa).run();
#if 0
		getLRLCP();
#endif
	}

	// returns matchlen * matchoff. Simple(O(p * lgn) performs better than super accelerant(O(p + lgn))
	// LCP array calculation is heavier than theoritical complexity(for small n)
	template<typename sIt>
	std::pair<size_t, size_t>
	Search(sIt sbegin, sIt send)
	{
		static_assert(detail::is_iterator_of<sIt, T>::value, "search string must contain same elem type as of the dictionary");
		if (n <= 1) {
			size_t l = 0;
			if (n != 0 && sbegin != send) {
				l = size_t(*sbegin == *begin);
			}
			return std::make_pair(l, 0);
		}
/*
		for (auto &e : sa) {
			auto pit = begin+e;
			while(pit != end) {
				std::cout << (char)*pit++;
			}
			std::cout << "\n";
		}
*/
		// d = search string length.
		size_t d = send-sbegin;

		// calculate lcp between begin+i and sbegin+j
		auto lcp_fn = [&](size_t i, size_t j) {
			size_t k = 0, p = std::min(n-i, d-j);
			for (k = 0; k < p; k++) {
				if (*(begin+i+k) != *(sbegin+j+k)) {
					return k;
				}
			}
			return p;
		};


		size_t l = 0, r = n-1; // lower, upper bound for binary search
		size_t f = 0; // begin+sa[f] is the matched ptr, p match length
		size_t ls = lcp_fn(sa[0], 0), rs = lcp_fn(sa[n-1], 0); // lcp(begin+sa[0], sbegin) and lcp(begin+sa[n-1], sbegin)
		if (ls == d || (d < n-sa[0] && *(sbegin+ls) <= *(begin+sa[0]+ls))) {
			f = 0;
		} else if (rs < d && (rs == n-sa[n-1] || *(sbegin+rs) > *(begin+sa[n-1]+rs))) {
			f = n-1;
		} else {
#if 0
			while(r-l > 1) {
				size_t m = 0;
				size_t c = (l+r)/2;
				if (ls >= rs) {
					if (llcp[c-1] >= ls) {
						m = ls + lcp_fn(sa[c]+ls, ls);
					} else {
						m = llcp[c-1];
					}
				} else {
					if (rlcp[c-1] >= rs) {
						m = rs + lcp_fn(sa[c]+rs, rs);
					} else {
						m = rlcp[c-1];
					}
				}
				if (m == d || (n-sa[c] > m && *(sbegin+m) <= *(begin+sa[c]+m))) {
					r = c;
					rs = m;
				} else {
					l = c;
					ls = m;
				}
			}
#else
			while(r-l > 1) {
				size_t c = (l+r)/2;
				auto m = std::min(ls, rs);
				m += lcp_fn(sa[c]+m, m);
				if (m == d || sa[c]+m == n) {
					auto suf_l = (n-sa[c]);
					if (d < suf_l) {
						rs = m;
						r = c;
					} else if (d > suf_l) {
						ls = m;
						l = c;
					} else {
						return std::make_pair(m, sa[c]);
					}
				} else if (*(begin+sa[c]+m) > *(sbegin+m)) {
					rs = m;
					r = c;
				} else {
					ls = m;
					l = c;
				}
			}
#endif
			f = (ls > rs)? l: r; // choose the bigger match
		}
		return std::make_pair(std::max(ls, rs), sa[f]);
	}
};
}

#endif
