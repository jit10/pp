#ifndef LLVM_DATADEP_ANAL_H 
#define LLVM_DATADEP_ANAL_H 
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

#include <unordered_map>
#include <vector>

namespace anal {
struct LlvmDataDepAnal : public llvm::FunctionPass {
	static char ID;

	LlvmDataDepAnal()
		: llvm::FunctionPass(ID) {}

	std::unordered_map<llvm::Instruction*, std::vector<llvm::Instruction*>> use_def_edges;
	std::vector<llvm::Instruction*> Ddep_instrs;

	void Instrument(llvm::Instruction* i);
	void Instrument(void);

	virtual void getAnalysisUsage(llvm::AnalysisUsage &au) const override {
		au.setPreservesAll();
	}

	virtual bool doInitialization(llvm::Module &m) override;
	virtual bool runOnFunction(llvm::Function &f) override;
	virtual void releaseMemory(void) override;

private:
	llvm::Value* ts_reg;
	llvm::Value* logddep_fn;
	std::unique_ptr<llvm::DataLayout> dl;

	// our friendly visitor.
	friend struct DataDepInstVisitor;
};
}

#endif
