#ifndef LLVM_PERSIST_H 
#define LLVM_PERSIST_H 
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringMap.h"

namespace anal {
struct LlvmPersist : public llvm::FunctionPass {
	static char ID;

	LlvmPersist()
		: llvm::FunctionPass(ID) {}

	llvm::StringMap<u32> unique_fids;

	virtual void getAnalysisUsage(llvm::AnalysisUsage &au) const override {
		au.setPreservesAll();
	}

	virtual bool doInitialization(llvm::Module &m) override;
	virtual bool runOnFunction(llvm::Function &f) override;
};
}

#endif
