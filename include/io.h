#ifndef IO_H
#define IO_H

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <cstddef>
#include <cstring>
#include <iterator>
#include <system_error>
#include <tuple>

#include "i.h"

#define MAX_VMEM 1ULL<<40

namespace io {
template<typename, typename, typename = void>
struct is_iterator_of : std::false_type {};

template<typename It, typename T>
struct is_iterator_of<It, T,
	typename std::enable_if_t<std::is_same<typename std::decay<decltype(*std::declval<It>())>::type, T>::value || std::is_assignable<decltype(*std::declval<It>()), decltype(std::declval<T>())>::value>>
	: std::true_type {};

template<typename, typename = void>
struct is_const_iterator : std::false_type {};

template<typename It>
struct is_const_iterator<It,
	typename std::enable_if_t<!std::is_assignable<decltype(*std::declval<It>()), decltype(*std::declval<It>())>::value>>
	: std::true_type {};

template<typename T>
struct size {
	static const size_t value;
};

template<typename T>
struct marshal {
	template<typename It>
	marshal(It buf, T const& x)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		const_cast<T&>(x).marshal(buf);
	}
};

template<typename T>
struct unmarshal {
	template<typename It>
	unmarshal(It buf, T& x)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		x.unmarshal(buf);
	}
};

template<>
struct size<u8> {
	static const size_t value = 1;
};

template<>
struct size<u16> {
	static const size_t value = 2;
};

template<>
struct size<u32> {
	static const size_t value = 4;
};

template<>
struct size<u64> {
	static const size_t value = 8;
};

template<>
struct marshal<u8> {
	template<typename It>
	marshal(It buf, const u8& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		*buf = u8(v);
	}
};

template<>
struct unmarshal<u8> {
	template<typename It>
	unmarshal(It buf, u8& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		v = *buf;
	}
};

// overloaded for u16, u32, u64 when It = u8* for x86 family(unaligned access allowed)
template<>
struct marshal<u16> {
	template<typename It>
	marshal(It buf, const u16& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		*buf = u8(v);
		*(++buf) = u8(v>>8);
	}

#if defined(__i386) || defined(__x86_64__) || defined(_M_IX86) || defined(_M_X64) 
	marshal(u8* buf, const u16& v)
	{
		*((u16*)(buf)) = v;
	}
#endif
};

template<>
struct unmarshal<u16> {
	template<typename It>
	unmarshal(It buf, u16& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		v = (u16)(*buf);
		v |= ((u16)(*(++buf))<<8);
	}

#if defined(__i386) || defined(__x86_64__) || defined(_M_IX86) || defined(_M_X64) 
	unmarshal(u8* buf, u16& v)
	{
		v = *((u16*)(buf));
	}
#endif
};

template<>
struct marshal<u32> {
	template<typename It>
	marshal(It buf, const u32& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		*buf = u8(v);
		*(++buf) = u8(v>>8);
		*(++buf) = u8(v>>16);
		*(++buf) = u8(v>>24);
	}

#if defined(__i386) || defined(__x86_64__) || defined(_M_IX86) || defined(_M_X64) 
	marshal(u8* buf, const u32& v)
	{
		*((u32*)(buf)) = v;
	}
#endif
};

template<>
struct unmarshal<u32> {
	template<typename It>
	unmarshal(It buf, u32& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		v = (u32)(*buf);
		v |= ((u32)(*(++buf))<<8);
		v |= ((u32)(*(++buf))<<16);
		v |= ((u32)(*(++buf))<<24);
	}

#if defined(__i386) || defined(__x86_64__) || defined(_M_IX86) || defined(_M_X64) 
	unmarshal(u8* buf, u32& v)
	{
		v = *((u32*)(buf));
	}
#endif
};

template<>
struct marshal<u64> {
	template<typename It>
	marshal(It buf, const u64& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		*buf = u8(v);
		*(++buf) = u8(v>>8);
		*(++buf) = u8(v>>16);
		*(++buf) = u8(v>>24);
		*(++buf) = u8(v>>32);
		*(++buf) = u8(v>>40);
		*(++buf) = u8(v>>48);
		*(++buf) = u8(v>>56);
	}

#if defined(__i386) || defined(__x86_64__) || defined(_M_IX86) || defined(_M_X64) 
	marshal(u8* buf, const u64& v)
	{
		*((u64*)(buf)) = v;
	}
#endif
};

template<>
struct unmarshal<u64> {
	template<typename It>
	unmarshal(It buf, u64& v)
	{
		static_assert(is_iterator_of<It, u8>::value, "iterator must be of u8 type");
		v = (u64)(*buf);
		v |= ((u64)(*(++buf))<<8);
		v |= ((u64)(*(++buf))<<16);
		v |= ((u64)(*(++buf))<<24);
		v |= ((u64)(*(++buf))<<32);
		v |= ((u64)(*(++buf))<<40);
		v |= ((u64)(*(++buf))<<48);
		v |= ((u64)(*(++buf))<<56);
	}

#if defined(__i386) || defined(__x86_64__) || defined(_M_IX86) || defined(_M_X64) 
	unmarshal(u8* buf, u64& v)
	{
		v = *((u64*)(buf));
	}
#endif
};

#define PIPE_BUF_SZ 64*1024

// Declare a named pipe(with read operation only for now).
template<typename T>
struct NamedPipe {
private:
	int fd;
	u8* b; // Small buffer.

public:
	// read iterator.
	struct riterator : std::iterator<std::input_iterator_tag, T> {
	private:
		NamedPipe<T>* pipe;
		u32 end;
		u32 pos;
		bool err;
		T v;

		std::pair<u64, int>
		blocking_read(int fd, u8* buf, u64 len)
		{
			int r;
			size_t n;
			n = r = 0;
			do {
				r = read(fd, buf+n, len-n);
				n += r < 0 ? 0 : r;
			} while(r < 0 && errno == EINTR);

			int err = r < 0 ? errno : 0;

			return std::make_pair(n, err);
		}

		void
		bread_T()
		{
			if (err) {
				return;
			}

			if (pos == end) {
				auto p = blocking_read(pipe->fd, pipe->b, sizeof pipe->b);
				if(p.first == 0 || p.second != 0) { // EOF or error => stop reading.
					err = true;
				}
				end = p.first;
				if (end < sizeof(pipe->b) && (end % io::size<T>::value != 0)) {
					err = true;
				}
				pos = 0;
			}

			if (!err) {
				unmarshal<T>(pipe->b+pos, v);
				pos += io::size<T>::value;
			}
		}

	public:
		riterator() : pipe(nullptr), end(0), pos(0), err(true), v() {}

		riterator(NamedPipe<T>& pipe)
		: pipe(&pipe), end(0), pos(0), err(false), v()
		{
			if (pipe.fd < 0) {
				err = true;
				return;
			}
			bread_T();
		}

		const T&
		operator*() const
		{
			return v;
		}

		const T*
		operator->() const
		{
			return &(operator*());
		}

		riterator&
		operator++()
		{
			bread_T();
			return *this;
		}

		riterator&
		operator++(int)
		{
			bread_T();
			return *this;
		}

		friend bool
		operator==(const riterator& i, const riterator& j)
		{
			return (i.err == j.err) && (i.err || (i.pipe == j.pipe && i.pos == j.pos && i.end == j.end));
		}

		friend bool
		operator!=(const riterator& i, const riterator& j)
		{
			return !(i == j);
		}
	};

	NamedPipe() : b(nullptr), fd(-1) {}

	NamedPipe(const std::string& name)
	{
		if ((fd = open(name.c_str(), O_RDONLY)) == -1) {
			throw std::system_error(errno, std::system_category());
		}

		b = new u8[PIPE_BUF_SZ * io::size<T>::value];
	}

	// No copy.
	NamedPipe(const NamedPipe& p) = delete;
	NamedPipe& operator=(const NamedPipe& p) = delete;

	// Move.
	NamedPipe(NamedPipe&& p)
	: fd(p.fd), b(p.b)
	{
		p.fd = -1;
		p.b = nullptr;
	}

	NamedPipe&
	operator=(NamedPipe&& p)
	{
		fd = p.fd;
		b = p.b;

		p.fd = -1;
		p.b = nullptr;

		return *this;
	}

	~NamedPipe()
	{
		// Invalid fd => no close().
		if (fd < 0) {
			return;
		}

		if (close(fd) == -1) {
			throw std::system_error(errno, std::system_category());
		}

		delete[] b;
	}

	NamedPipe::riterator
	rbegin() { return riterator(*this); }

	NamedPipe::riterator
	rend() { return riterator(); }
};

#undef PIPE_BUF_SZ

struct MmapFile {
private:
	int fd;
	u8* mem;
	u64 vmem;
	u64 grow;
	u64 mend;
	u64 size;
	bool err;

	int
	extend(u64 nsize)
	{
		if (nsize > mend) {
			mend = std::max(mend + grow, nsize);
			if (ftruncate(fd, mend) == -1) {
				return errno;
			}
		}

		return 0;
	}

	u64
	writeAt(u64 pos, u8* buf, u64 len)
	{
		if (err) {
			return 0;
		}

		u64 npos = pos + len;
		if (extend(npos) != 0) {
			err = true;
			return 0;
		}

		std::memcpy(mem+pos, buf, len);
		size = std::max(size, npos);
		return len;
	}


public:
	template<typename T, bool>
	struct iterator_tpl;

	template<typename T> using iterator = iterator_tpl<T, true>;
	template<typename T> using const_iterator = iterator_tpl<T, false>;

	template<typename T, bool is_nonconst>
	struct iterator_tpl : std::iterator<std::random_access_iterator_tag, T> {
	private:
		MmapFile* f;
		u64 pos;

		struct deref_proxy {
		private:
			iterator_tpl* const it;
	
		public:
			deref_proxy(iterator_tpl* it) : it(it) {}

			void
			operator=(const T& v)
			{
				if (it->f->err) {
					return;
				}

				if (it->f->extend(it->pos+io::size<T>::value) != 0) {
					it->f->err = true;
					return;
				}

				marshal<T>(it->f->mem+it->pos, v);
				it->f->size = std::max(it->f->size, it->pos+io::size<T>::value);
			}
	
		private:
			void operator =(deref_proxy const &);
		};

		bool
		ok() const
		{
			return !f->err;
		}

		friend iterator_tpl<T, !is_nonconst>;

	public:
		iterator_tpl(MmapFile& f, u64 pos = 0)
		: f(&f), pos(pos)
		{}

		iterator_tpl() : f(nullptr), pos(0) {}

		template<bool b = !is_nonconst, typename = std::enable_if_t<b>>
		iterator_tpl(const iterator<T>& it)
		: f(it.f), pos(it.pos)
		{}

		template<bool b = is_nonconst, typename = std::enable_if_t<b>>
		iterator_tpl(const const_iterator<T>& it)
		: f(it.f), pos(it.pos)
		{}

		size_t
		hash() const
		{
			return std::hash<decltype(f->mem)>()(f->mem+pos);
		}

		template<bool b = is_nonconst, typename = std::enable_if_t<b>>
		deref_proxy
		operator*()
		{
			return deref_proxy(this);
		}

		template<bool b = !is_nonconst, typename = std::enable_if_t<b>>
		T
		operator*() const
		{
			T v;
			unmarshal<T>(f->mem+pos, v);

			return v;
		}

		iterator_tpl&
		operator++()
		{
			pos += io::size<T>::value;
			return *this;
		}

		iterator_tpl&
		operator++(int)
		{
			pos += io::size<T>::value;
			return *this;
		}

		iterator_tpl&
		operator--()
		{
			pos -= io::size<T>::value;
			return *this;
		}

		iterator_tpl&
		operator--(int)
		{
			pos -= io::size<T>::value;
			return *this;
		}

		iterator_tpl
		operator+(const std::ptrdiff_t n) const
		{
			return iterator_tpl(*f, pos+n*io::size<T>::value);
		}

		iterator_tpl
		operator-(const std::ptrdiff_t n) const
		{
			return iterator_tpl(*f, pos-n*io::size<T>::value);
		}

		iterator_tpl&
		operator+=(const std::ptrdiff_t n)
		{
			pos += n*io::size<T>::value;
			return *this;
		}

		iterator_tpl&
		operator-=(const std::ptrdiff_t n)
		{
			pos -= n*io::size<T>::value;
			return *this;
		}

		std::ptrdiff_t
		operator-(const iterator_tpl& it) const
		{
			return (pos-it.pos)/io::size<T>::value;
		}

		template<bool b = is_nonconst, typename = std::enable_if_t<b>>
		deref_proxy
		operator[](const std::ptrdiff_t n)
		{
			return deref_proxy(this+n);
		}

		template<bool b = !is_nonconst, typename = std::enable_if_t<b>>
		T
		operator[](const std::ptrdiff_t n) const
		{
			T v;
			unmarshal<T>(f->mem+pos+n*io::size<T>::value, v);

			return v;
		}

		friend bool
		operator==(const iterator_tpl& x, const iterator_tpl& y)
		{
			return (x.ok() == y.ok()) && (!x.ok() || (x.f == y.f && x.pos == y.pos)); // end may differ.
		}

		friend bool
		operator!=(const iterator_tpl& x, const iterator_tpl& y)
		{
			return !(x == y);
		}

		friend bool
		operator<(const iterator_tpl& x, const iterator_tpl& y)
		{
			return (x.ok() == y.ok()) && (!x.ok() || (x.f == y.f && x.pos < y.pos));
		}
		
		friend bool
		operator>(const iterator_tpl& x, const iterator_tpl& y)
		{
			return (x.ok() == y.ok()) && (!x.ok() || (x.f == y.f && x.pos > y.pos));
		}

		friend bool
		operator<=(const iterator_tpl& x, const iterator_tpl& y)
		{
			return (x == y) || (x < y);
		}

		friend bool
		operator>=(const iterator_tpl& x, const iterator_tpl& y)
		{
			return (x == y) || (x > y);
		}
	};

	MmapFile() : fd(-1), mem(nullptr), vmem(0), grow(0), size(0), err(true) {}
	MmapFile(const std::string& name, bool overwrite = false, u64 vmem = MAX_VMEM, u64 grow = 16*1024)
	: vmem(vmem), grow(grow), mend(0), size(0), err(false)
	{
		if ((fd = open(name.c_str(), O_CREAT|O_RDWR, 0644)) == -1) {
			err = true;
			throw std::system_error(errno, std::system_category());
		}

		if (overwrite) {
			size = 0;
		} else {
			struct stat st;
			if (fstat(fd, &st) == -1) {
				err = true;
				throw std::system_error(errno, std::system_category());
			}
			size = st.st_size;
		}

		mend = size;

		if((mem = static_cast<u8*>(mmap(NULL, vmem, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0))) == (u8*)(-1)) {
			err = true;
			throw std::system_error(errno, std::system_category());
		}
	}

	// No copy.
	MmapFile(const MmapFile& m) = delete;
	MmapFile& operator=(const MmapFile& m) = delete;

	// Move.
	MmapFile(MmapFile&& m)
	: fd(m.fd), mem(m.mem), vmem(m.vmem), grow(m.grow), mend(m.mend), size(m.size), err(m.err)
	{
		m.fd = -1;
	}

	MmapFile&
	operator=(MmapFile&& m)
	{
		fd = m.fd;
		mem = m.mem;
		vmem = m.vmem;
		grow = m.grow;
		mend = m.mend;
		size = m.size;
		err = m.err;

		m.fd = -1;

		return *this;
	}

	~MmapFile()
	{
		// invalid fd => nothing to cleanup.
		if (fd == -1) {
			return;
		}

		if (ftruncate(fd, size) == -1) {
			throw std::system_error(errno, std::system_category());
		}

		if (msync(mem, size, MS_SYNC) == -1) {
			throw std::system_error(errno, std::system_category());
		}

		if (munmap(mem, vmem) == -1) {
			throw std::system_error(errno, std::system_category());
		}

		if (close(fd) == -1) {
			throw std::system_error(errno, std::system_category());
		}
	}

	bool empty() const { return size == 0; }
	const u8& operator[](u64 n) const { return *(mem+n); }

	size_t length() const { return size; }

	void
	commit()
	{
		if (ftruncate(fd, size) == -1) {
			throw std::system_error(errno, std::system_category());
		}

		mend = size;

		if (msync(mem, size, MS_SYNC) == -1) {
			throw std::system_error(errno, std::system_category());
		}
	}

	void resize(size_t n) { size = n; }

	template<typename T> decltype(auto) cbegin() { return const_iterator<T>(*this); }
	template<typename T> decltype(auto) cend() { auto it = const_iterator<T>(*this, size); return it; }

	template<typename T> decltype(auto) begin() { return iterator<T>(*this); }
	template<typename T> decltype(auto) end() { auto it = iterator<T>(*this, size); return it; }
};
}

namespace std {
template<typename T, bool b>
struct hash<io::MmapFile::iterator_tpl<T, b>> {
	size_t operator()(const io::MmapFile::iterator_tpl<T, b>& it) const
	{
		return it.hash();
	}
};
}

#endif
