#ifndef PATHPROF_H
#define PATHPROF_H

#include <iostream>

#include <unordered_set>
#include <utility>

#include "common.h"
#include "graph.h"

namespace std {
template<typename N>
struct hash<std::pair<N, N>> {
	// copy-pasta boost hash_combine for std::pair<N, N>.
	inline void
	hash_combine(std::size_t& seed, N const& v) const
	{
		seed ^= std::hash<N>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
	}

	size_t
	operator()(std::pair<N, N> const& p) const
	{
		std::size_t seed = 0;
		hash_combine(seed, p.first);
		hash_combine(seed, p.second);
		return seed;
	}
};
}

namespace pp {
using namespace graph;

// Edge kinds.
enum struct PPEdgeKind : size_t { REAL, FAKE_BACK, FAKE_OTHER, INVALID };

// Edge properties.
struct PPEdge {
	PPEdgeKind k;
	u64 val;
	u64 inc;

	PPEdge(PPEdgeKind k = PPEdgeKind::REAL, u64 val = 0, u64 inc = 0)
		: k(k), val(val), inc(inc) {}
};

// Instrumentation types and their positions.
enum struct InstrKind : size_t {INCR = 0, LOG, INIT, REAL, BACK};
enum struct InstrPos : size_t {BEGIN = 0, END};

template<typename Node>
class InstrTab {
	std::function<void(Node, u64 val, InstrPos)> incr;
	std::function<void(Node, InstrPos)> log;
	std::function<void(Node, u64 val, InstrPos)> init;
	std::function<void(Node, Node, u64 val)> real;
	std::function<void(Node, Node, u64 in_val, u64 out_val)> back;

	// right order of enum values
	std::tuple<decltype(incr), decltype(log), decltype(init), decltype(real), decltype(back)> funcs;

public:
	InstrTab()
	{
		funcs = std::make_tuple(incr, log, init, real, back);
	}

	InstrTab(decltype(incr) incr, decltype(log) log, decltype(init) init, decltype(real) real, decltype(back) back) :
		incr(incr), log(log), init(init), real(real), back(back)
	{
		funcs = std::make_tuple(incr, log, init, real, back);
	}

	template<InstrKind k, typename... Args>
	void
	Invoke(Args... args)
	{
		std::get<static_cast<int>(k)>(funcs)(args...);
	}
};

template<typename Node>
struct PathProf : Graph<Node, PPEdge> {
	using typename Graph<Node, PPEdge>::INode;
	using Graph<Node, PPEdge>::G;
	using typename Graph<Node, PPEdge>::nodefunc_t;
	using typename Graph<Node, PPEdge>::edgefunc_t;

	void Traverse(nodefunc_t f, edgefunc_t b = [](Node& src, Node& tgt){});
	void PostOrderTraverse(nodefunc_t f, edgefunc_t b = [](Node& src, Node& tgt){});

	const Node& Entry() const { return this->entry; }
	const std::unordered_set<Node>& Exits() const { return this->exits; }
	u64 NumPaths() const { return numpaths; }

	void Process(const std::vector<std::pair<Node, Node>>& f);
	void Process() { auto f = std::vector<std::pair<Node, Node>>(); Process(f); };

	void ComputeVal();
	void ComputeInc();
	void Instrument(InstrTab<Node>&);
	void Print(std::function<std::string(const Node&)>);

	PathProf(const Node& e)
	: numpaths(0), entry(e)
	{
		G[entry] = std::move<std::vector<std::unique_ptr<INode>>>({});
	}

private:
	u64 numpaths;
	Node entry, exit;

	// Needed for encoding. Leaves = leaves union exits.
	std::unordered_set<Node> leaves;
	std::unordered_set<Node> exits;

	// Collected for efficient instrumentation.
	std::vector<std::pair<Node, Node>> back_edges;
	std::unordered_map <std::pair<Node, Node>, std::vector<PPEdge>> Vals;
	std::unordered_map <std::pair<Node, Node>, std::vector<PPEdge>> Incs;
};

template<typename N>
void
PathProf<N>::Print(std::function<std::string(const N&)> f) {
	std::cerr << "NumPaths : " << numpaths << "\n";
	for (auto &g : G) {
		for (auto& c : g.second) {
			std::cerr << "Edge : " << f(g.first) << " -> " << f(c->v)
			<< " Val : " << c->e.val << " Inc : " << c->e.inc <<  " Kind : " << static_cast<size_t>(c->e.k) << "\n";
		}
	}
}

template<typename N>
void
PathProf<N>::PostOrderTraverse(nodefunc_t f, edgefunc_t b)
{
	Graph<N, PPEdge>::PostOrderTraverse(entry, f, b);
}

template<typename N>
void
PathProf<N>::Traverse(nodefunc_t f, edgefunc_t b)
{
	Graph<N, PPEdge>::Traverse(entry, f, b);
}

// fakelist : list <fresh, term>, where
// term : representative of the path terminating at that node : Hence term->exit edge.
// fresh : representative of the begining of new path : Hence entry->fresh edge.
// models several underlying control flow.(call, longjump, ind).
template<typename N>
void
PathProf<N>::Process(const std::vector<std::pair<N, N>>& fakelist)
{
	// First create invalid edges from src -> tgt in fakelist. Necessary to form a connected graph.
	for(auto &p : fakelist) {
		this->CreateEdge(p.first, p.second, PPEdge(PPEdgeKind::INVALID));
	}

	u64 count = 0;
	this->PostOrderTraverse(
		[this, &count](N& c) {
			// First node in postorder traversal is exit. 
			if (count++ == 0) {
				exit = c;
			}

			// This is a function exit.
			if (G.find(c) == G.end()) {
				exits.insert(c);
			}
		},
		[this, count](N& src, N& tgt) {
			// Backedge from a leaf node.
			if(G[src].size() == 1) {
				leaves.insert(src);
			}

			for (auto& t : G[src]) {
				if (t->v == tgt) {
					t->e.k = PPEdgeKind::INVALID;
				}
			}

			// Remember this backedge.
			back_edges.push_back(std::make_pair(src, tgt));
		}
	);

	// FakeEdge : <src, tgt> : entry -> tgt & src -> exit.

	// Create fake edge for the backedges.
	for (auto &p : back_edges) {
		if (p.first == exit && p.second == entry) {
			return;
		}

		if (entry != p.second) {
			this->CreateEdge(entry, p.second, PPEdge(PPEdgeKind::FAKE_BACK));
		}
		if (exit != p.first) {
			this->CreateEdge(p.first, exit, PPEdge(PPEdgeKind::FAKE_BACK));
		}
	}

	// Now create fake edges for fakelist. 
	for(auto &p : fakelist) {
		if (entry != p.second) {
			this->CreateEdge(entry, p.second, PPEdge(PPEdgeKind::FAKE_OTHER));
		}
		if (p.first != exit) {
			this->CreateEdge(p.first, exit, PPEdge(PPEdgeKind::FAKE_OTHER));
		}
	}
}

template<typename N>
void
PathProf<N>::ComputeVal()
{
	std::unordered_map<N, u64> paths;
	this->PostOrderTraverse([this, &paths](N& c) {
		if (leaves.find(c) != leaves.end() || exits.find(c) != exits.end()) {
			paths[c] = 1;
		} else {
			u64 n = 0;
			for (auto& t : G[c]) {
				// skip invalid edges.
				if (t->e.k == PPEdgeKind::INVALID) {
					continue;
				}

				t->e.val = n;
				if (n != 0 || t->e.k != PPEdgeKind::REAL) { // we need 0 valued edges which are not just incr(aka real).
					Vals[std::make_pair(c, t->v)].push_back(t->e);
				}
				n += paths[t->v];
			}
			paths[c] = n;
		}
	});

	numpaths = paths[entry];
}

template<typename N>
void
PathProf<N>::ComputeInc()
{
}

template<typename N>
void
PathProf<N>::Instrument(InstrTab<N>& instrs)
{
	// Initialize path register = 0.
	instrs.template Invoke<InstrKind::INIT>(entry, 0, InstrPos::BEGIN);

	// collect the real edge instruments for later.
	std::unordered_map<std::pair<N, N>, u64> real_vals;
	// collect the return block instruments for later.
	std::unordered_map<N, u64> ret_vals;

	// single block instrumentation first.
	for (auto &ne : Vals) {
		for (auto &e : ne.second) {
			if (e.k == PPEdgeKind::REAL) {
				real_vals[ne.first] = e.val;
			} else if (e.k == PPEdgeKind::FAKE_OTHER) {
				if (ne.first.first == entry) {	// return block instr before call block(hence the collection).
					ret_vals[ne.first.second] = e.val;
				} else if (ne.first.second == exit) {	// call block => incr reg -> log.
					// Order matters.
					instrs.template Invoke<InstrKind::LOG>(ne.first.first, InstrPos::BEGIN);
					instrs.template Invoke<InstrKind::INCR>(ne.first.first, e.val, InstrPos::BEGIN);
				}
			}
		}
	}

	// return block reg reset.(also single block)
	for (auto &ne : ret_vals) {
		instrs.template Invoke<InstrKind::INIT>(ne.first, ne.second, InstrPos::BEGIN);
	}

	// log at the exit.(also single block)
	for (auto &p : exits) {
		instrs.template Invoke<InstrKind::LOG>(p, InstrPos::END);
	}

	// edge instrumentation always at the end or else it messes with the graph.
	// Technically we can just create newblocks do the instrument there and ordering won't matter.
	// But llvm does it the smart way, so we maintain the order.

	// back edge instrument.
	for (auto &p : back_edges) {
		u64 in_val, out_val;
		auto f = [this](N n1, N n2) {
			u64 v = 0;
			auto ne = Vals.find(std::make_pair(n1, n2));
			if (ne != Vals.end()) {
				for (auto &e : ne->second) {
					if (e.k == PPEdgeKind::FAKE_BACK) {
						v = e.val;
					}
				}
			}
			return v;
		};
		in_val = f(entry, p.second);
		out_val = f(p.first, exit);
		if (in_val != 0 || out_val != 0) {
			instrs.template Invoke<InstrKind::BACK>(p.first, p.second, in_val, out_val);
		}
	}

	// forward edge instrumentation.
	for (auto &ev : real_vals) {
		instrs.template Invoke<InstrKind::REAL>(ev.first.first, ev.first.second, ev.second);
	}
}
}

#endif
