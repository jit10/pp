#ifndef LLVM_VALANAL_H 
#define LLVM_VALANAL_H 

#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

namespace anal {
struct LlvmValAnal : public llvm::FunctionPass {
	static char ID;

	LlvmValAnal()
		: llvm::FunctionPass(ID) {}

	std::vector<llvm::Instruction*> Val_instrs;

	void Instrument(llvm::Instruction* i);
	void Instrument(void);

	virtual void getAnalysisUsage(llvm::AnalysisUsage &au) const override {
		au.setPreservesAll();
	}

	virtual bool doInitialization(llvm::Module &m) override;
	virtual bool runOnFunction(llvm::Function &f) override;
	virtual void releaseMemory(void) override;

private:
	llvm::Value* logvalN_fn[4];
	llvm::Value* logval_fn;
	std::unique_ptr<llvm::DataLayout> dl;
};
}

#endif
