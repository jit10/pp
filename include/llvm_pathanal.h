#ifndef LLVM_PATH_ANAL_H 
#define LLVM_PATH_ANAL_H 
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

#include "pathprof.h"
#include "llvm_persist.h"

#include <functional>

namespace anal {
struct LlvmPathAnal : public llvm::FunctionPass {
	static char ID;

	LlvmPathAnal()
		: llvm::FunctionPass(ID) {}

	std::unique_ptr<pp::PathProf<llvm::BasicBlock*>> ppenc_graph;

	void Instrument(void);

	virtual void getAnalysisUsage(llvm::AnalysisUsage &au) const override {
		au.addRequired<LlvmPersist>();
		au.setPreservesAll();
	}

	virtual bool doInitialization(llvm::Module &m) override;
	virtual bool runOnFunction(llvm::Function &f) override;
	virtual void releaseMemory(void) override;

private:
	u32 fid;
	std::vector<llvm::Instruction*> setjmp_rets;
	std::vector<llvm::Instruction*> long_jmps;
	std::vector<std::pair<llvm::BasicBlock*, llvm::BasicBlock*>> self_loops;

	std::function<void(llvm::Instruction*, u8, u64)> symlogf;
	pp::InstrTab<llvm::BasicBlock*> instr_tab;

	std::unordered_set<llvm::BasicBlock*> split_blocks(llvm::Function &f);
};
}

#endif
