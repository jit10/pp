#ifndef LLVM_CDEP_ANAL_H
#define LLVM_CDEP_ANAL_H
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Analysis/PostDominators.h"

#include <unordered_map>

namespace anal {
struct LlvmCtrlDepAnal : public llvm::FunctionPass {
	static char ID;

	LlvmCtrlDepAnal()
		: llvm::FunctionPass(ID) {}

	std::vector<llvm::BasicBlock*> bps, ipds;

	void Instrument(void);

	virtual void getAnalysisUsage(llvm::AnalysisUsage &au) const override {
		au.addRequired<llvm::PostDominatorTree>();
		au.setPreservesAll();
	}

	virtual bool doInitialization(llvm::Module &m) override;
	virtual bool runOnFunction(llvm::Function &f) override;
	virtual void releaseMemory(void) override;

private:
	std::unordered_map<llvm::BasicBlock*, u32> bbids;
	llvm::BasicBlock* entry;
	llvm::PostDominatorTree* PDT;
	llvm::Type* int32Ty;
	llvm::Value* frame_addr;
	llvm::Value *frame_fn, *branching_fn, *merging_fn, *log_fn;
};
}
#endif
