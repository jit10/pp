#ifndef TRACE_H
#define TRACE_H

#include <cassert>
#include <string>

#include "io.h"
#include "common.h"

namespace trace {
enum Compression : u8 {
	NONE,
	BFCM,
	DEDUP,
};

struct TraceHeader {
	std::string prog;
	Compression c;
	TraceType t;
	u8 id;

	TraceHeader() {}
	TraceHeader(std::string prog, Compression c, TraceType t, u8 id) : prog(prog), c(c), t(t), id(id) {}

	size_t size();

	template<typename It>
	void
	marshal(It buf)
	{
		u32 l = 0;
		std::copy(prog.begin(), prog.end(), buf);
		buf[prog.length()] = '\0';
		l += prog.length()+1;
		io::marshal<u8>(buf+l, c);
		l += io::size<u8>::value;
		io::marshal<u8>(buf+l, t);
		l += io::size<u8>::value;
		io::marshal<u8>(buf+l, id);
	}

	template<typename It>
	void
	unmarshal(It buf)
	{
		u32 l = 0;
		auto buf_s = buf;
		while (*buf++ != '\0') {}
		prog.assign(buf_s, buf);
		l += prog.length()+1;
		io::unmarshal<u8>(buf+l, reinterpret_cast<u8&>(c));
		l += io::size<u8>::value;
		io::unmarshal<u8>(buf+l, reinterpret_cast<u8&>(t));
		l += io::size<u8>::value;
		io::unmarshal<u8>(buf+l, id);
	}
};

struct Trace {
	std::string path;
	TraceHeader hdr;
	io::MmapFile f;

	Trace();
	Trace(std::string path); // open for read
	Trace(std::string path, TraceHeader hdr); // open for writing
};
}

#endif
