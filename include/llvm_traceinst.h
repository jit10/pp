#ifndef LLVM_TRACEINST_H 
#define LLVM_TRACEINST_H 
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

#include "llvm_pathanal.h"
#include "llvm_ddepanal.h"
#include "llvm_valanal.h"

namespace inst {
struct LlvmTraceInst : public llvm::FunctionPass {
	static char ID;

	LlvmTraceInst()
		: llvm::FunctionPass(ID) {}

	virtual void getAnalysisUsage(llvm::AnalysisUsage &au) const override {
		au.addRequired<anal::LlvmValAnal>();
		au.addRequired<anal::LlvmDataDepAnal>();
		au.addRequired<anal::LlvmPathAnal>();
	}

	virtual bool doInitialization(llvm::Module &m) override;
	virtual bool runOnFunction(llvm::Function &m) override;
};
}

#endif
