#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

// needed to write all iterator glue code; just use boost cb.
#include <boost/circular_buffer.hpp>

template<typename T>
using sliding_window = boost::circular_buffer<T>;

#endif
