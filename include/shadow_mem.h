#ifndef SHADOW_MEM_H
#define SHADOW_MEM_H

// Only supported for PIE binary on amd64 linux.(-fPIE -pie)
// TODO: 32-bit

#define SHADOW_BEGIN 0x100000000000ULL
#define SHADOW_END 0x500000000000ULL

#define SHADOW_OFFSET 0xb00000000000ULL

#define MEM2SHADOW(addr) (((addr & ~0x3ULL)<<1)-SHADOW_OFFSET)

#endif
