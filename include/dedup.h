#ifndef DEDUP_H
#define DEDUP_H

#include <array>
#include <algorithm>
#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>
#include <openssl/sha.h>

#include "common.h"
#include "circular_buffer.h"
#include "farmhash.h"
#include "io.h"
#include "suffix.h"

#include <iostream>
namespace dedup {
#define UHASH_INTERNAL_BUFFER_LENGTH 8*1024ULL

struct uhash {
	typedef std::array<u8, SHA_DIGEST_LENGTH> type;

private:
	u64 _pos;
	u8 _buf[UHASH_INTERNAL_BUFFER_LENGTH];
	SHA_CTX ctx;
	type h;

public:
	uhash() : _pos(0), _buf(), h() { SHA1_Init(&ctx); }

	template<typename T>
	void operator()(T v)
	{
		if (io::size<T>::value + _pos <= UHASH_INTERNAL_BUFFER_LENGTH) {
			io::marshal<T>(_buf+_pos, v);
			_pos += io::size<T>::value;
		} else {
			SHA1_Update(&ctx, _buf, _pos);
			u8 b[io::size<T>::value];
			io::marshal<T>(b, v);
			SHA1_Update(&ctx, b, sizeof b);
			_pos = 0;
		}
	}

	void operator()(type v)
	{
		SHA1_Update(&ctx, v.data(), v.size());
	}

	template<typename It>
	void operator()(It begin, It end)
	{
		static_assert(io::is_iterator_of<It, u8>::value, "iterator must be of u8 type");

		auto len = end - begin;
		if (len + _pos <= UHASH_INTERNAL_BUFFER_LENGTH) {
			std::copy(begin, end, _buf+_pos);
			_pos += len;
		} else {
			SHA1_Update(&ctx, _buf, _pos);
			auto b = std::make_unique<u8[]>(len);
			std::copy(begin, end, b.get());
			SHA1_Update(&ctx, b.get(), len);
			_pos = 0;
		}
	}

	type
	get()
	{
		if (_pos) {
			SHA1_Update(&ctx, _buf, _pos);
		}
		SHA1_Final(h.data(), &ctx);
		return h;
	}
};

#undef UHASH_INTERNAL_BUFFER_LENGTH

template<typename Sym>
bool is_start(const Sym& s);

template<typename Sym>
bool is_end(const Sym& s);

template<typename Sym>
bool is_ptr(const Sym& s);

template<typename Sym>
bool is_cpr(const Sym& s);

template<typename Sym>
size_t cpr_len(const Sym& s);

template<typename Sym>
size_t cpr_off(const Sym& s);

template<typename Sym>
Sym mk_cpr_sym(size_t len, size_t off);

template<typename It>
size_t
balanced_count(It begin, It end)
{
	size_t l = 0, r = 0, nbal = 0;
	for(auto it = begin; it != end; it++) {
		if (is_start(*it)) { //change
			l++;
		} else if (is_end(*it)) { //change
			r++;
		}
		if (l == r) {
			nbal = (it-begin) + 1;
		}
	}

	return nbal;
}

// implements LZFG.
template<typename Sym, size_t N>
struct LZCompressor {
private:
	// virtual input pointer to the dictionary.(pos, pos+dict.size() is our sliding window)
	size_t pos;
	// sliding window of virtual pointers corresponding to phrase boundaries within dictionary.
	sliding_window<size_t> phrases;
	sliding_window<Sym> dict;

public:
	LZCompressor() : pos(0), phrases(N), dict(N)  {}

	template<typename InIt, typename OutIt>
	size_t
	compress(InIt begin, InIt end, OutIt out)
	{
		static_assert(io::is_iterator_of<OutIt, u8>::value, "input iterator must be of u8 type");
		static_assert(io::is_iterator_of<InIt, u8>::value, "output iterator must be of u8 type");
		assert((end-begin)%io::size<Sym>::value == 0 && "serialized input data len not multiple of Sym size");

		auto out_begin = out;
		sliding_window<Sym> lab(N);

		auto fill_lab = [&]() {
			while(begin != end && lab.size() < lab.capacity()) {
				Sym s;
				io::unmarshal<decltype(s)>(begin, s);
				lab.push_back(s);
				std::advance(begin, io::size<Sym>::value);
			}
		};

		// first fill the entire look-ahead buffer.
		fill_lab();
		if (lab.size() == 0) {
			return 0;
		}

		while (lab.size() > 0) {
			size_t l = 0, o = 0;
			if (lab.size() > 1) {
				auto p = suffixarray::Dictionary<decltype(dict.begin())>(dict.begin(), dict.end()).Search(lab.begin(), lab.end());
				l = p.first; o = p.second;
			} else {
				l = 1; // lab size = 1 => just write the literal.
			}

			// can do binary search since phrases is always sorted.
			auto pit = std::lower_bound(phrases.begin(), phrases.end(), pos+o);
			l = balanced_count(dict.begin()+o, dict.begin()+o+l);
			if (phrases.size() == 0 || l == 0 || pit == phrases.end() || pos+o < *pit) { // match pointer not phrase boundary
				*out = *(lab.begin());
				l = size_t(1);
			} else {
				// repeatitive pattern (L > D)
				if (o+l == dict.size()) {
					auto l_i = l;
					auto lit1 = lab.begin(), lit2 = lab.begin() + l;
					while(lit2 != lab.end() && *lit1 == *lit2) {
						l++; lit1++; lit2++;
					}
					auto nbal = balanced_count(lit2 - (l%l_i), lit2);
					l = l + nbal - (l%l_i);
				}
				// LZFG distance is measured in phrases.
				if (l == 1) {
					*out = *(lab.begin());
				} else {
					*out = mk_cpr_sym<Sym>(l, size_t(phrases.end() - pit));
					std::cout << "match : " << l << " parsed : " << pos/1024 << "K\n";
				}
			}
			std::advance(out, io::size<Sym>::value);

			/* If we have to drop elems from the front.
			We make sure the dictionary starts with phrase boundary.
			Also phrases is updated accordingly. */
			auto next_pos = pos+dict.size();
			if (dict.size() > N-l) {
				auto pit = std::lower_bound(phrases.begin(), phrases.end(), pos+(dict.size()+l-N));
				if (pit == phrases.end()) { // l = N
					dict.clear();
					pos = next_pos;
				} else {
					dict.erase_begin(*pit - pos);
					pos = *pit;
				}
				phrases.erase_begin(pit - phrases.begin());
			}
			phrases.push_back(next_pos);
			dict.insert(dict.end(), lab.begin(), lab.begin()+l);
			lab.erase_begin(l);
			fill_lab(); // advances begin as necessary.
		}

		return (out-out_begin);
	}

	template<typename InIt, typename OutIt>
	void
	decompress(InIt begin, InIt end, OutIt out)
	{
		sliding_window<Sym> lab(N); // temporary buffer to hold expanded compressed elements.
		for (auto it = begin; it != end;) {
			Sym s;
			io::unmarshal<decltype(s)>(it, s);

			size_t l = 0;
			if (is_cpr(s)) {
				l = cpr_len(s);
				size_t o = cpr_off(s);
				assert(o <= phrases.size() && "invalid back-reference encountered while decompressing");
				auto k = (*(phrases.end()-o) - pos);
				auto dit = dict.begin() + k;
				for (auto i = 0; i < l; i++) {
					auto cs = *(dit+i%(dict.size()-k));
					lab.push_back(cs);

					io::marshal<Sym>(out, cs);
					std::advance(out, io::size<Sym>::value);
				}
			} else {
				l = 1;
				lab.push_back(s);

				io::marshal<Sym>(out, s);
				std::advance(out, io::size<Sym>::value);
			}
			phrases.push_back(pos+dict.size());
			if (dict.size() > N-l) {
				pos += l;
			}
			dict.insert(dict.end(), lab.begin(), lab.begin()+l);
			lab.erase_begin(l);

			std::advance(out, io::size<Sym>::value);
		}
	}
};

#ifdef WITH_DEBUG
template<typename Sym>
struct dedup_profile_info_t {
private:
	std::vector<Sym> stack;
	std::pair<u64, u64> ratio;

public:
	struct block_info {
		u64 freq;
		u64 dedup_freq;
		u64 cur_size;
		u64 max_size;
		u64 total_size;

		block_info() : freq(0), dedup_freq(0), cur_size(0), max_size(0), total_size(0) {}
	};

	std::unordered_map<Sym, block_info> block_freq;
	size_t max_depth;
	u64 max_dedup_chunk;
	u64 hits;

	dedup_profile_info_t() : stack(), ratio(), block_freq(), max_depth(0), max_dedup_chunk(0), hits(0) {}

	void observe(const Sym& s, s64 orig_incr, s64 dedup_incr, bool hit)
	{
		ratio.first = s64(ratio.first) + orig_incr;
		ratio.second = s64(ratio.second) + dedup_incr;

		auto id = s;
		if (is_start(s)) {
			stack.push_back(id);
			block_freq[id].freq++;
			max_depth = std::max(max_depth, stack.size());
		} else if (is_end(s)) {
			id = stack.back();
			stack.pop_back();
		} else {
			if (stack.empty()) {
				return;
			}
			id = stack.back();
		}

		// Following needs fixing for non-tail recursive functions.
		if (is_end(s)) {
			if (hit) {
				hits++;
				block_freq[id].dedup_freq++;
				block_freq[id].cur_size = io::size<Sym>::value;
				max_dedup_chunk = std::max(dedup_incr < 0 ? u64(-dedup_incr) : u64(dedup_incr), max_dedup_chunk);
			} else {
				block_freq[id].cur_size += io::size<Sym>::value;
			}
			block_freq[id].max_size = std::max(block_freq[id].max_size, block_freq[id].cur_size);
			block_freq[id].total_size += block_freq[id].cur_size;
			block_freq[id].cur_size = 0;
		} else {
			block_freq[id].cur_size += io::size<Sym>::value;
		}
	}

	double get_ratio() { return double(ratio.first)/double(ratio.second); }
};
#endif

template<typename Sym, typename It, typename C, typename Ptr = Sym>
uhash::type
block_hash(It begin, It end, const std::vector<It>& childs, std::function<Ptr(It)> it2ptr)
{
	uhash h;
	int idx = 0;
	for(auto ctx = begin; ctx != end;) {
		typename C::template const_iterator<Sym> cit(ctx);
		auto s = *cit;
		if (is_start(s) && ctx != begin) { // non-dedup child tree = hash it's ptr.
			assert(!childs.empty() && "childs can't be empty"); // begin != leaf node in call tree.
			h(it2ptr(ctx));	// hash of the ptr.
			ctx = childs[idx]; // skip the sub-tree.
			idx++;
		} else {
			h(s);
			ctx++;
		}
	}

	return h.get();
}

template<typename Ptr>
using dedup_table_t = std::unordered_map<uhash::type, Ptr>;

template<typename Sym, typename InIt, typename OutIt, typename OutC>
struct DedupCompressor {
	static_assert(io::is_iterator_of<OutIt, Sym>::value, "Output iterator must have the same type as Sym");
	static_assert(io::is_iterator_of<InIt, Sym>::value, "Input iterator must have the same type as Sym");

	typedef Sym Ptr;

private:
	OutIt out_begin;
	OutIt out;
	std::vector<OutIt> callstack;
	std::unordered_map<OutIt, std::vector<OutIt>> childs;
	std::function<Ptr(OutIt)> it2ptr;
	dedup_table_t<Ptr>& hcons_tbl;

#ifdef WITH_DEBUG
	dedup_profile_info_t<Sym> prof;
#endif

public:
	DedupCompressor(dedup_table_t<Ptr>& hcons_tbl, OutIt out, std::function<Ptr(OutIt)> it2ptr)
	: out_begin(out), out(out), it2ptr(it2ptr), hcons_tbl(hcons_tbl) {}

	void add(const Sym& s);
	void compress(InIt begin, InIt end);

	u64 size() { return io::size<Sym>::value * (out - out_begin); }

#ifdef WITH_DEBUG
	dedup_profile_info_t<Sym> get_profile_info() { return prof; }
#endif
};

template<typename Sym, typename InIt, typename OutIt, typename OutC>
void
DedupCompressor<Sym, InIt, OutIt, OutC>::compress(InIt begin, InIt end)
{
	for (; begin != end; begin++) {
		auto sym = *begin;
		add(sym);
	}
}

template<typename Sym, typename InIt, typename OutIt, typename OutC>
void
DedupCompressor<Sym, InIt, OutIt, OutC>::add(const Sym& sym)
{
	auto diff = io::size<Sym>::value;

	*out++ = sym;

	if (is_start(sym)) {
		callstack.push_back(out-1);

#ifdef WITH_DEBUG
		prof.observe(sym, diff, diff, false);
#endif

	} else if (is_end(sym)) {
		assert(!callstack.empty() && "callstack can't be empty");
		auto cur = callstack.back();
		callstack.pop_back();

		// The trace should not contain single bb leaf nodes.(i.e. E()L() with no path ids)(TODO:BOGUS?)
		if ((out - cur) == 2) {
			out = cur;
			return;
		}

		uhash::type hv;
		if (childs.find(cur) == childs.end()) {
			hv = block_hash<Sym, OutIt, OutC>(cur, out, std::vector<OutIt>(), it2ptr);
		} else {
			hv = block_hash<Sym, OutIt, OutC>(cur, out, childs[cur], it2ptr);
		}

		childs.erase(cur);

		if (hcons_tbl.find(hv) != hcons_tbl.end()) {
#ifdef WITH_DEBUG
		prof.observe(sym, diff, -(io::size<Sym>::value*(out-cur) - io::size<Ptr>::value), true);
#endif
			out = cur;
			*out++ = hcons_tbl[hv];
		} else {
			if (!callstack.empty()) {
				auto parent = callstack.back();
				childs[parent].push_back(out);
			}
			hcons_tbl[hv] = it2ptr(cur);
#ifdef WITH_DEBUG
			prof.observe(sym, diff, diff, false);
#endif
		}
	} else {
#ifdef WITH_DEBUG
		prof.observe(sym, diff, diff, false);
#endif
	}
}

// build hashconsing tbl.(needs to be run over each trace part of set)
template<typename Sym, typename It, typename C, typename Ptr = Sym>
void
dedup_parse(dedup_table_t<Ptr>& hcons_tbl, It begin, It end, std::function<Ptr(It)> it2ptr)
{
	static_assert(io::is_iterator_of<It, Sym>::value, "Iterator must have the same type as Sym");
	static_assert(io::is_const_iterator<It>::value, "Iterator must not be assignable");

	std::vector<It> callstack;
	std::unordered_map<It, std::vector<It>> childs;

	for (auto i = begin; i != end;) {
		auto s = *i++;

		if (is_start(s)) {
			callstack.push_back(i-1);
		} else if (is_end(s)) {
			assert(!callstack.empty() && "callstack can't be empty");
			auto cur = callstack.back();
			callstack.pop_back();

			uhash::type hv;
			if (childs.find(cur) == childs.end()) {
				hv = block_hash<Sym, It, C>(cur, i, std::vector<It>(), it2ptr);
			} else {
				hv = block_hash<Sym, It, C>(cur, i, childs[cur], it2ptr);
			}

			childs.erase(cur);

			if (hcons_tbl.find(hv) == hcons_tbl.end()) {
				if (!callstack.empty()) {
					auto parent = callstack.back();
					childs[parent].push_back(i);
				}
				hcons_tbl[hv] = it2ptr(cur);
			}
		}
	}
}
}

namespace std {
template<>
struct hash<dedup::uhash::type> {
	size_t operator()(const dedup::uhash::type& h) const
	{
		return util::Hash64(reinterpret_cast<const char*>(h.data()), h.size());
	}
};
}

#endif
