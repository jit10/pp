#ifndef DEDUP_TRACE_H
#define DEDUP_TRACE_H

#include <sys/types.h>
#include <dirent.h>

#include "dedup.h"
#include "trace.h"

namespace trace {
typedef u64 FilePtr;

FilePtr mkFilePtr(u8 id, u64 pos)
{
	// assert(pos & (0xfffULL << 52) == 0 && "file size too big");
	return ((u64)id << 52) ^ pos; // top 4 bits left for flags.
}

struct TraceSet {
private:
	TraceType t;
	std::string prog;
	std::vector<Trace> traces;
	dedup::dedup_table_t<FilePtr> hcons_tbl;

public:
	TraceSet(std::string prog, TraceType t);

	dedup::dedup_table_t<FilePtr>& cons_table();

	void load(std::string path);

	Trace& create(std::string path);

	template<typename It>
#ifdef WITH_DEBUG
	dedup::dedup_profile_info_t<u64>
#else
	void
#endif
	record(It begin, It end, std::string path)
	{
		traces.push_back(Trace(path, TraceHeader(prog, DEDUP, t, traces.size())));
		auto& t = traces.back();
		io::MmapFile::iterator<u64> fb(t.f, t.hdr.size());

		auto cpr = dedup::DedupCompressor<u64, It, decltype(fb), decltype(t.f)>(hcons_tbl, fb,
			[fb, &t](decltype(fb) it) {
				return FilePtr(mk_trace_sym(mkFilePtr(t.hdr.id, it - fb), TraceSym::I));
			});

		cpr.compress(begin, end);

		t.f.resize(cpr.size() + t.hdr.size());
		t.f.commit();
#ifdef WITH_DEBUG
		return cpr.get_profile_info();
#endif
	}
};
}

#endif

