#include <cassert>

#include <memory>

#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace graph {
// Simple Adjacency list graph structure.
template<typename Node, typename Edge>
struct Graph {
	struct INode {
		Edge e;
		Node v;

		INode (const Node& v, const Edge& e) : e(e), v(v) {}
	};

	typedef std::function<void(Node&)> nodefunc_t;
	typedef std::function<void(Node&, Node&)> edgefunc_t;

	void CreateEdge(const Node& src, const Node& tgt, const Edge& e);
	void CreateEdge(const Node& src, const Node& tgt);

	// Forward traverse the graph and call f() on the nodes and b() on backedges while traversing.
	// Don't modify the graph, in f() or b().
	void Traverse(Node& root, nodefunc_t f, edgefunc_t b);

	// PostOrder traverse the graph call f() on the nodes and b() on backedges while traversing.
	// Don't modify the graph, in f() or b().
	void PostOrderTraverse(Node& root, nodefunc_t f, edgefunc_t b);

	Graph() {}

	~Graph() {}

protected:
	std::unordered_map<Node, std::vector<std::unique_ptr<INode>>> G;
};

template<typename N, typename E>
void
Graph<N, E>::CreateEdge(const N& src, const N& tgt, const E& e)
{
	G[src].push_back(std::make_unique<INode>(tgt, e));
}

template<typename N, typename E>
void
Graph<N, E>::CreateEdge(const N& src, const N& tgt)
{
	E e;
	G[src].push_back(std::make_unique<INode>(tgt, e));
}

template<typename N, typename E>
void
Graph<N, E>::Traverse(N& root, nodefunc_t f, edgefunc_t b)
{
	std::unordered_set<N> seen;
	std::unordered_set<N> stack;
	nodefunc_t helper = 
		[this, f, b, &helper, &seen, &stack](N& cur) {
		seen.insert(cur);
		stack.insert(cur);
		f(cur);
		auto succs = G.find(cur);
		if (succs != G.end()) {
			for (auto& t : succs->second) {
				if (seen.find(t->v) == seen.end()) {
					helper(t->v);
				} else if (stack.find(t->v) != stack.end()) {
					b(cur, t->v);
				}
			}
		}
		stack.erase(cur);
	};

	helper(root);
}

template<typename N, typename E>
void
Graph<N, E>::PostOrderTraverse(N& root, nodefunc_t f, edgefunc_t b)
{
	std::unordered_set<N> seen;
	std::unordered_set<N> stack;
	nodefunc_t helper =
		[this, f, b, &helper, &seen, &stack](N& cur) {
		seen.insert(cur);
		stack.insert(cur);
		auto succs = G.find(cur);
		if (succs != G.end()) {
			for (auto& t : succs->second) {
				if (seen.find(t->v) == seen.end()) {
					helper(t->v);
				} else if (stack.find(t->v) != stack.end()) {
					b(cur, t->v);
				}
			}
		}
		f(cur);
		stack.erase(cur);
	};

	helper(root);
}
}
