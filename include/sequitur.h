#ifndef SEQITUR_H
#define SEQITUR_H

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <cassert>

#include "common.h"

using namespace std;

namespace sequitur {
	template <typename Sym>
   	struct Sequitur {
		bool lookahead;
		Sym start;
		function<Sym()> gen_sym;

		struct Alist {
			u32 refs;
			vector<Sym> vtx;
			unordered_set<Sym> pars;
		};
		//non-terminal symbols
		unordered_map<Sym, Alist*> grammar;

		void Process(Sym x);
		
		Sequitur(bool lookahead, std::function<Sym()> gen_sym): lookahead(lookahead), gen_sym(gen_sym) {
			grammar[start = gen_sym()] = new Alist{};
			max_alist = 2;
		};

		~Sequitur() {
			print();

			// destroy objects.
			for (auto &g : grammar) {
				delete(g.second);
			}
			grammar.clear();
		}

	private:
		u32 max_alist;
		void print();
		Sym reduce();
		void subst(Sym s, Sym y); // [G(y)/y]s 
	};

	template <typename Sym>
	void
	Sequitur<Sym>::print() 
	{
#ifdef WITH_DEBUG
		for (auto &kv : grammar) {
			cout << "NT : " << kv.first << " refs : " << kv.second->refs << " parents : ";
			for (auto &i : kv.second->pars) {
				cout << i << ",";
			}
			cout << " Alist : ";
			for (auto &i : kv.second->vtx) {
				cout << i << " ~> ";
			}
			cout << "\n";
		}
#endif
	}

	template <typename Sym>
	s32
	find_didup(vector<Sym> v)
	{
		// Have to make sure not to count overlapping dup digrams.
		if (v.size() < 4)
			return -1;

		auto m = v[v.size()-2];
		auto n = v[v.size()-1];
		for (auto i = 0; i <= v.size()-4; i++) {
			if (v[i] == m && v[i+1] == n) {
				return i;
			}
		}
		return -1;
	}
	
	template <typename Sym>
	Sym
	Sequitur<Sym>::reduce()
	{
		auto s = grammar[start];
		auto f = [] (vector<Sym> vs, vector<Sym> vn) {
			auto s = vs.size();
			auto n = vn.size();
			if (s < n) {
				return false;
			}
			for (s32 i = n-1; i >= 0; i--) {
				if (vn[i] != vs[i+s-n]) {
					return false;
				}
			}
			return true;
		};

		auto r = start;
		auto n = 0UL;
		// Find the max length non-term to reduce.
		for (auto &kv : grammar) {
			if (kv.first != start && f(s->vtx, kv.second->vtx)) {
				if (kv.second->vtx.size() > n) {
					r = kv.first;
					n = kv.second->vtx.size();
				} else if (n == max_alist) {
					break;
				}
			}
		}
	
		return r;
	}

	template <typename Sym>
	void
	Sequitur<Sym>::subst(Sym s, Sym y)
	{
		//create a replacement vector with production of y replaced in s.
		vector<Sym> nvtx;
		for (auto &i : grammar[s]->vtx) {
			if (i == y) {
				for (auto &j : grammar[y]->vtx) {
					nvtx.push_back(j);
				}
			} else {
				nvtx.push_back(i);
			}
		}
	
		// fix parents set for the non-terms in production of y.
		for (auto &i : grammar[y]->vtx) {
			if (grammar.find(i) != grammar.end()) {
				grammar[i]->pars.erase(y);
				grammar[i]->pars.insert(s);
			}
		}

		//Keep track of potentially longest chain to reduce.
		if (nvtx.size() > max_alist) {
			max_alist = nvtx.size();
		}
		grammar[s]->vtx = nvtx;
	}
	
	template <typename Sym>
	void
	Sequitur<Sym>::Process(Sym x)
	{
		if (lookahead) {
		}
		
		auto s = grammar[start];
		s->vtx.push_back(x);

		// Nothing to do.
		if (s->vtx.size() < 4)
			return;

		auto nsym = reduce();
		if (nsym != start) {
			grammar[nsym]->refs += 1;
			grammar[nsym]->pars.insert(start);
	
			auto f = [this, nsym] (Sym m) {
				if (grammar.find(m) != grammar.end()) {
					auto al = grammar[m];
					al->pars.insert(nsym);
					al->refs--;
					// This makes removing the parent edge from the reduced NTs without scanning S.
					if (al->pars.size() > al->refs) {
						al->pars.erase(start);
					}
				}
			};

			// call f on each removed sym and finally add the reduced NT.
			for (auto i = 0; i < grammar[nsym]->vtx.size(); i++){
				f(s->vtx[s->vtx.size()-1]);
				s->vtx.pop_back();
			}
			s->vtx.push_back(nsym);
		}
	
		auto m = s->vtx[s->vtx.size()-2];
		auto n = s->vtx[s->vtx.size()-1];

		// invariant = atmost one digram.
		auto dup = find_didup(s->vtx);
		if (dup >= 0) {
			auto nsym = gen_sym();
			grammar[nsym] = new Alist{2, vector<Sym>{m, n}};
			grammar[nsym]->pars.insert(start);

			// Nice lambda C++. LOL.
			auto f = [this, nsym] (Sym m) {
				if (grammar.find(m) != grammar.end()) {
					auto al = grammar[m];
					al->pars.insert(nsym);
					al->refs--; 
					// This makes removing the parent edge from the reduced NTs without scanning S.
					if (al->pars.size() > al->refs) {
						al->pars.erase(start);
					}
				}
			};
			
			// Check if any of the symbols from the dup digram are NTs.
			f(m);
			f(n);
	
			// reduce the recent digram.
			s->vtx.pop_back(); s->vtx.pop_back();
			s->vtx.push_back(nsym);

			//reduce the dup digram.
			s->vtx[dup+1] = nsym;
			for (auto i = dup; i < s->vtx.size(); i++){ s->vtx[i] = s->vtx[i+1]; }
			s->vtx.pop_back();
		}

		// invariant = more than one occurence of non-terms.
		for (auto kv = grammar.cbegin(); kv != grammar.cend();) {
			if (kv->first != start && kv->second->refs <= 1) {
				assert(kv->second->pars.size() == kv->second->refs);
				for (auto &elem : kv->second->pars) {
					subst(elem, kv->first);
				}
				delete(kv->second);
				kv = grammar.erase(kv);
			} else {
				kv++;
			}
		}
	}
}
#endif
