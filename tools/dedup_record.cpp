#include <iostream>

#include "llvm/Support/CommandLine.h"

#include "io.h"
#include "dedup_trace.h"

using namespace std;
using namespace llvm;
using namespace io;
using namespace trace;

cl::opt<string> Fifo("fifo", cl::desc("Fifo path"), cl::value_desc("fifo"), cl::Required);
cl::opt<string> Prog("prog", cl::desc("unique program name"), cl::value_desc("prog"), cl::Required);
cl::opt<string> Out("o", cl::desc("output trace path"), cl::value_desc("filename"), cl::Required);

void
record_pathtrace(std::string fifo, std::string out)
{
	auto p = NamedPipe<u64>(fifo);
	auto traces = TraceSet(Prog, PATH);
#ifdef WITH_DEBUG
	auto prof = traces.record(p.rbegin(), p.rend(), out);
	cout << "Compression ratio : " << prof.get_ratio() << "\n";
	cout << "Max chunk compressed : " << prof.max_dedup_chunk << "\n";
	cout << "Max callstack depth : " << prof.max_depth << "\n";
	cout << "Hits : " << prof.hits << "\n";
	for (auto& p : prof.block_freq) {
		cout << "Function : " << (p.first & ~(0x7ULL << 61)) << "\n\tTotal Freq : " << p.second.freq << "\n\tDedup Freq : " << p.second.dedup_freq << "\n\tAvg. Size : " << p.second.total_size/p.second.freq << "\n\tMax Size : " << p.second.max_size << "\n";
	}
#else
	traces.record(p.rbegin(), p.rend(), out);
#endif
}

int
main(int argc, char** argv)
{
	cl::ParseCommandLineOptions(argc, argv);
	record_pathtrace(Fifo, Out);
}
