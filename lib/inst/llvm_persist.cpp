#include "llvm/Support/Debug.h"
#include "llvm/Support/CommandLine.h"

#include <fstream>
#include "i.h"

#include "llvm_persist.h"

#define DEBUG_TYPE "persist"

using namespace llvm;
using namespace std;

char anal::LlvmPersist::ID = 0;
static RegisterPass<anal::LlvmPersist> X("persist", "Collect persistent data");

namespace anal {
static cl::opt<string> Fid_store("dump-fids", cl::init(""), cl::Optional,
		cl::desc("file to store(if empty)/load unique function ids"));

// Assumption IR parser fills the function list same order every run.(iteration order remains same)
bool
LlvmPersist::doInitialization(Module &m)
{
	DEBUG(errs() << "In persist initialization" << "\n");
	auto fid = 0UL;

	fstream fd(Fid_store, ios::in);
	if (fd.good()) {
		string fname;
		do {
			fd >> fname >> fid;
			if (fd.eof()) {
				break;
			} else {
				DEBUG(errs() << fname << " : " << fid << "\n");
				unique_fids[fname] = fid;
			}
		} while(1);
	} else {
		for (auto &f : m) {
			if (!f.empty()) {
				unique_fids[f.getName()] = ++fid;
			}
		}

		fstream fd(Fid_store, ios::out|ios::trunc);
		if (fd.is_open()) {
			for (auto &p : unique_fids) {
				fd << p.first().str() << "\t" << p.second << "\n";
			}
			fd.close();
		}
	}

	return true;
}

bool
LlvmPersist::runOnFunction(Function &f)
{
	return false;
}
}

