#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"

#include "common.h"
#include "shadow_mem.h"
#include "llvm_ddepanal.h"

#define DEBUG_TYPE "datadep_anal"

using namespace llvm;
using namespace std;

char anal::LlvmDataDepAnal::ID = 0;
static RegisterPass<anal::LlvmDataDepAnal> X("datadep-anal", "Analysis to trace data dependencies");

namespace anal {
static cl::opt<bool> DDepInst_test("datadep-test", cl::init(false), cl::Hidden,
	cl::desc("Enable datadep instrumentation for testing"));

struct DataDepInstVisitor : InstVisitor<DataDepInstVisitor> {
	LlvmDataDepAnal& da;

	DataDepInstVisitor(LlvmDataDepAnal& da) : da(da) {}

	void
	instr_store(Instruction& i, Value* addr, u64 sz, u64 align)
	{
		DEBUG(errs() << "Store instrumentation for : " << i << "\n");

		sz = ROUNDUP(sz, 4);
		if (sz == 0) {
			return;
		}
		IRBuilder<> irb(&i);

		auto* int64Ty = irb.getInt64Ty();

		assert(addr->getType()->isPointerTy() && "PointerOperand not pointer!!");
		auto mem = irb.CreatePtrToInt(addr, int64Ty);

		// We only record dependencies on word granularity.
		if (align < 4) {
			mem = irb.CreateAnd(mem, ~0x3ULL);
		}

		// MEM2SHADOW(mem).
		mem = irb.CreateShl(mem, 1);
		auto shadow = irb.CreateSub(mem, ConstantInt::get(int64Ty, SHADOW_OFFSET, false));

		// Record current timestamp.
		auto ts = irb.CreateLoad(da.ts_reg);
		auto shmem = irb.CreateIntToPtr(shadow, ts->getType()->getPointerTo());
		irb.CreateStore(ts, shmem);

		// If we have more, we just update the corresponding shadow memory. 
		for (u64 j = 4; j < sz; j+=4) {
			shmem = irb.CreateConstGEP1_32(shmem, 1);
			irb.CreateStore(ts, shmem);
		}
	}

	void
	visitStoreInst(StoreInst &i)
	{
		auto sz = da.dl->getTypeStoreSize(i.getValueOperand()->getType());
		auto addr = i.getPointerOperand();
		instr_store(i, addr, sz, i.getAlignment());
	}

	void
	visitLoadInst(LoadInst &i)
	{
		DEBUG(errs() << "Load instrumentation for : " << i << "\n");

		IRBuilder<> irb(&i);
		auto mem = irb.CreatePtrToInt(i.getPointerOperand(), irb.getInt64Ty());
		Value* args[] = {
			mem,
		};
		irb.CreateCall(da.logddep_fn, args);
	}

	void
	visitMemIntrinsic(MemIntrinsic &i)
	{
		auto sz = i.getLength();
		auto addr = i.getRawDest();
		// If the size is constant, we just instrument. This should cover all the initialization case.
		if(ConstantInt* csz = dyn_cast<ConstantInt>(sz)) {
			instr_store(i, addr, csz->getZExtValue(), i.getAlignment());
		} // TODO: instrument a call otherwise.
	}
};

void
LlvmDataDepAnal::Instrument(Instruction* i)
{
	DataDepInstVisitor v(*this);
	v.visit(i);
}

void
LlvmDataDepAnal::Instrument(void)
{
	DataDepInstVisitor v(*this);
	v.visit(Ddep_instrs.begin(), Ddep_instrs.end());
}

bool
LlvmDataDepAnal::doInitialization(Module &m)
{
	auto &ctx = m.getContext();
	auto* int64Ty = Type::getInt64Ty(ctx);
	auto* voidTy = Type::getVoidTy(ctx);

	dl = std::make_unique<DataLayout>(&m);
	ts_reg = m.getOrInsertGlobal(UNQ_NAME(ts_reg), int64Ty);
	logddep_fn =  m.getOrInsertFunction(UNQ_NAME(log_datadep), voidTy, int64Ty, nullptr);

	auto* alloc_fn = m.getOrInsertFunction(UNQ_NAME(shadow_alloc), voidTy, nullptr);
	auto* free_fn = m.getOrInsertFunction(UNQ_NAME(shadow_free), voidTy, nullptr);
	appendToGlobalCtors(m, cast<Function>(alloc_fn), 0);
	appendToGlobalDtors(m, cast<Function>(free_fn), 65535);

	return true;
}

bool
LlvmDataDepAnal::runOnFunction(Function &f)
{
	DEBUG(errs() << "Data Dependence Analysis for : " << f.getName() << "\n");
	// TODO: Opt.(AliasAnalysis) to figure out what to exclude from instrumentation.
	for (auto i = inst_begin(f); i != inst_end(f); i++) {
		if (isa<LoadInst>(*i)) {
			Ddep_instrs.push_back(&*i);
			continue;
		}

		// We consider MemIntrinsics as store. TODO: other intrinsics?
		if (isa<StoreInst>(*i) || isa<MemIntrinsic>(*i)) {
			Ddep_instrs.push_back(&*i);
		}

		for (Use &u : i->operands()) {
			if (Instruction* ui = dyn_cast<Instruction>(u.get())) {
				use_def_edges[&*i].push_back(ui);
			}
		}
	}

	// In test mode modify IR.
	if (DDepInst_test) {
		Instrument();
	}

	return DDepInst_test;
}

void
LlvmDataDepAnal::releaseMemory(void)
{
	Ddep_instrs.clear();
	use_def_edges.clear();
}
}
