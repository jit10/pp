# A bit of a sanity check:
if( NOT EXISTS ${LLVM_ROOT}/include/llvm )
message(FATAL_ERROR "LLVM_ROOT (${LLVM_ROOT}) is not a valid LLVM install")
endif()

# our api usage is based on 3.6 future llvm update will need change in headers.
if ( NOT ${LLVM_PACKAGE_VERSION} VERSION_EQUAL "3.6" )
	message(FATAL_ERROR "${LLVM_ROOT} doesn't contain required LLVM version 3.6")
endif()

# We incorporate the CMake features provided by LLVM:
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${LLVM_ROOT}/share/llvm/cmake")
include(LLVMConfig)
include(AddLLVM)

option(LLVM_ENABLE_CXX11  "Enable C++11" ON)
option(LLVM_INCLUDE_TOOLS "Generate build targets for the LLVM tools." ON)
option(LLVM_BUILD_TOOLS
  "Build the LLVM tools. If OFF, just generate build targets." ON)

# Now set the LLVM header and library paths:
add_definitions( ${LLVM_DEFINITIONS} )
include_directories( ${LLVM_INCLUDE_DIRS} )
link_directories( ${LLVM_LIBRARY_DIRS} )

# And the project header and library paths
link_directories( ${LIBRARY_OUTPUT_PATH} )

add_library(traceinst SHARED
	llvm_persist.cpp
	llvm_cdepanal.cpp
	llvm_ddepanal.cpp
	llvm_valanal.cpp
	llvm_pathanal.cpp
	llvm_traceinst.cpp
)

