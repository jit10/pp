#include "llvm/IR/Constants.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CommandLine.h"

#include "common.h"
#include "llvm_cdepanal.h"

#define DEBUG_TYPE "ctrldep_anal"

using namespace llvm;
using namespace std;

char anal::LlvmCtrlDepAnal::ID = 0;
static RegisterPass<anal::LlvmCtrlDepAnal> X("ctrldep-anal", "Analysis to trace control dependencies");

namespace anal {
static cl::opt<bool> CDepInst_test("ctrldep-test", cl::init(false), cl::Hidden,
	cl::desc("Enable control-dep instrumentation for testing"));

bool
LlvmCtrlDepAnal::doInitialization(Module &m)
{
	DEBUG(errs() << "In Control Dep Analysis\n");
	auto &ctx = m.getContext();
	int32Ty = Type::getInt32Ty(ctx);
	auto* int8PtrTy = Type::getInt8PtrTy(ctx);
	auto* voidTy = Type::getVoidTy(ctx);

	frame_addr = m.getOrInsertGlobal(UNQ_NAME(cur_frame_addr), int8PtrTy);
	branching_fn = m.getOrInsertFunction(UNQ_NAME(branching), voidTy, int32Ty, int32Ty, nullptr);
	merging_fn = m.getOrInsertFunction(UNQ_NAME(merging), voidTy, int32Ty, nullptr);
	log_fn =  m.getOrInsertFunction(UNQ_NAME(log_ctrldep), voidTy, nullptr);
	frame_fn = Intrinsic::getDeclaration(&m, Intrinsic::frameaddress);
	return true;
}

// TODO: setjmp/longjmp handling.
void
LlvmCtrlDepAnal::Instrument(void)
{
	// Instrumentation order : frameaddr -> logging -> merging -> branching. (whichever is required).

	// branching.
	for (auto &bb : bps) {
		auto bb_id = bbids[bb];
		auto ipd_id = bbids[PDT->getNode(bb)->getIDom()->getBlock()];
		Value *args[] = {
			ConstantInt::get(int32Ty, bb_id, false),
			ConstantInt::get(int32Ty, ipd_id, false),
		};
		CallInst::Create(branching_fn, args, "", bb->getFirstInsertionPt());
	}

	//merging.
	for (auto &bb : ipds) {
		auto ipd_id = bbids[bb];
		Value *args[] = {
			ConstantInt::get(int32Ty, ipd_id, false),
		};
		CallInst::Create(merging_fn, args, "", bb->getFirstInsertionPt());
	}

	// logging.
	for (auto &e : bbids) {
		CallInst::Create(log_fn, "", e.first->getFirstInsertionPt());
	}

	// frameaddr.
	IRBuilder<> irb(entry->getFirstInsertionPt());
	Value* args[] = {
		ConstantInt::get(int32Ty, 0, false),
	};
	auto fp = irb.CreateCall(frame_fn, args);
	irb.CreateStore(fp, frame_addr);
}

bool
LlvmCtrlDepAnal::runOnFunction(Function &f)
{
	DEBUG(errs() << "Control Dependence Analysis for : " << f.getName() << "\n");

	if (f.empty()) {
		return false;
	}

	entry = &f.getEntryBlock();

	// Assumption IR parser fills the BB list same order every run.(iteration order remains same).
	auto i = 0UL;
	for (auto &bb : f) {
		// unique bb id within a function.
		bbids.insert(std::make_pair(&bb, ++i));

		auto* i = bb.getTerminator();
		if (i == NULL) {
			continue;
		}
		if (i->getNumSuccessors() > 1) {
			bps.push_back(&bb);
		}
	}

	DEBUG(errs() << "BPs:\n");
	for (auto &bb: bps) {
		DEBUG(errs() << "\t" << bb->getName() << "\n");
	}

	PDT = &getAnalysis<PostDominatorTree>();
	for(auto node = GraphTraits<PostDominatorTree*>::nodes_begin(PDT); node != GraphTraits<PostDominatorTree*>::nodes_end(PDT); ++node) {
		if (node->getNumChildren()) {
			ipds.push_back(node->getBlock());
		}
	}

	DEBUG(errs() << "IPDs:\n");
	for (auto &bb: ipds) {
		DEBUG(errs() << "\t" << bb->getName() << "\n");
	}

	// In test mode modify IR.
	if (CDepInst_test) {
		Instrument();
	}

	return CDepInst_test;
}

void
LlvmCtrlDepAnal::releaseMemory(void)
{
	bps.clear();
	ipds.clear();
	bbids.clear();
}
}
