#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CommandLine.h"

#include <unordered_set>
#include <unordered_map>

#include "common.h"
#include "llvm_valanal.h"

#define DEBUG_TYPE "val_anal"

using namespace llvm;
using namespace std;

char anal::LlvmValAnal::ID = 0;
static RegisterPass<anal::LlvmValAnal> X("val-anal", "Analysis of values to trace");

namespace anal {
static cl::opt<bool> ValInst_test("val-test", cl::init(false), cl::Hidden,
	cl::desc("Enable value instrumentation for testing"));

void
LlvmValAnal::Instrument(Instruction* i)
{
	BasicBlock::iterator iter(i);
	// Phi nodes must be grouped at the top of BB.
	while (isa<PHINode>(*(++iter))) {
		++iter;
	};
	auto irb = IRBuilder<>(iter);
	Value* args[1];

	auto castFn = [&irb, &args, &i](u8 sz) {
		if (i->getType()->isPointerTy()) {
			args[0] = irb.CreatePtrToInt(i, irb.getIntNTy(sz*8));
		} else {
			args[0] = irb.CreateZExtOrBitCast(i, irb.getIntNTy(sz*8));
		}
	};
	switch (dl->getTypeStoreSize(i->getType())) {
	case 1:
		castFn(1);
		irb.CreateCall(logvalN_fn[0], args);
		break;

	case 2:
		castFn(2);
		irb.CreateCall(logvalN_fn[1], args);
		break;

	case 4:
		castFn(4);
		irb.CreateCall(logvalN_fn[2], args);
		break;

	case 8:
		castFn(8);
		irb.CreateCall(logvalN_fn[3], args);
		break;

	default:
		auto lval = irb.CreateAlloca(i->getType());
		irb.CreateStore(i, lval);
		auto vptr = irb.CreateBitCast(lval, irb.getInt8PtrTy());
		Value* largs[] = {
			vptr,
			ConstantInt::get(irb.getInt8Ty(), dl->getTypeStoreSize(i->getType()), false),
		};
		irb.CreateCall(logval_fn, largs);
		break;
	}
}

void
LlvmValAnal::Instrument(void)
{
	for (auto i : Val_instrs) {
		Instrument(i);
	}
}

bool
LlvmValAnal::doInitialization(Module &m)
{
	auto &ctx = m.getContext();

	dl = std::make_unique<DataLayout>(&m);
	logval_fn = m.getOrInsertFunction(UNQ_NAME(log_val), Type::getVoidTy(ctx), Type::getInt8PtrTy(ctx), Type::getInt8Ty(ctx), nullptr);
	logvalN_fn[0] = m.getOrInsertFunction(UNQ_NAME(log_val8), Type::getVoidTy(ctx), Type::getInt8Ty(ctx), nullptr);
	logvalN_fn[1] = m.getOrInsertFunction(UNQ_NAME(log_val16), Type::getVoidTy(ctx), Type::getInt16Ty(ctx), nullptr);
	logvalN_fn[2] = m.getOrInsertFunction(UNQ_NAME(log_val32), Type::getVoidTy(ctx), Type::getInt32Ty(ctx), nullptr);
	logvalN_fn[3] = m.getOrInsertFunction(UNQ_NAME(log_val64), Type::getVoidTy(ctx), Type::getInt64Ty(ctx), nullptr);

	return true;
}

bool
LlvmValAnal::runOnFunction(Function &f)
{
	DEBUG(errs() << "Value Analysis for : " << f.getName() << "\n");

	// TODO: can do some more analysis to filter values to exclude from instrumentation.
	for (auto ii = inst_begin(f); ii != inst_end(f); ii++) {
		if (ii->getType()->isVoidTy()) {
			continue;
		}

		Val_instrs.push_back(&*ii);
	}

	// Test mode: modify IR.
	if (ValInst_test) {
		Instrument();
	}

	return ValInst_test;
}

void
LlvmValAnal::releaseMemory(void)
{
	Val_instrs.clear();
}
}
