#include "llvm/Support/Debug.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"

#include "common.h"
#include "llvm_traceinst.h"

using namespace anal;
using namespace llvm;
using namespace std;

#define DEBUG_TYPE "trace_inst"

char inst::LlvmTraceInst::ID = 0;
static RegisterPass<inst::LlvmTraceInst> X("trace-inst", "Instrumentation for whole execution trace");

namespace inst {
bool
LlvmTraceInst::doInitialization(Module &m)
{
	auto voidTy = Type::getVoidTy(m.getContext());

	auto* init_fn = m.getOrInsertFunction(UNQ_NAME(rt_init), voidTy, nullptr);
	auto* fini_fn = m.getOrInsertFunction(UNQ_NAME(rt_fini), voidTy, nullptr);
	appendToGlobalCtors(m, cast<Function>(init_fn), 0);
	appendToGlobalDtors(m, cast<Function>(fini_fn), 65535);

	return true;
}

bool
LlvmTraceInst::runOnFunction(Function &f)
{
	// Very nice LLVM. Nice PassManager.
//	auto &da = getAnalysis<LlvmDataDepAnal>();
//	auto &va = getAnalysis<LlvmValAnal>();
	auto &pa = getAnalysis<LlvmPathAnal>();

//	da.Instrument();
//	va.Instrument();
	pa.Instrument();

	return true;
}
}
