#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/CallSite.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

#include <string>
#include <unordered_set>

#include "common.h"
#include "llvm_pathanal.h"

#define DEBUG_TYPE "path_anal"

using namespace pp;
using namespace llvm;
using namespace std;

char anal::LlvmPathAnal::ID = 0;
static RegisterPass<anal::LlvmPathAnal> x("path-anal", "Analysis of paths");

namespace anal {
static cl::opt<bool> PathInst_test("path-test", cl::init(false), cl::Hidden,
		cl::desc("Enable path instrumentation for testing"));

BasicBlock*
SplitEdgeWrapper(BasicBlock* bb, BasicBlock* succ, Pass* P)
{
	auto* t = bb->getTerminator();
	unsigned e = t->getNumSuccessors();
	if (e == 1) {
		if (t->getSuccessor(0) != succ) {
			bb = t->getSuccessor(0);
		}
	}

	t = bb->getTerminator();
	e = t->getNumSuccessors();

	for (unsigned i = 0; ; ++i) {
		if (i == e) {
			for (unsigned i = 0; i < e; ++i) {
				if (t->getSuccessor(i)->getTerminator()->getNumSuccessors() == 1 && t->getSuccessor(i)->getTerminator()->getSuccessor(0) == succ) {
					succ = t->getSuccessor(i);
					break;
				}
			}
			break;
		} else if (t->getSuccessor(i) == succ) {
			break;
		}
	}
	return SplitEdge(bb, succ, P);
}

bool
is_skip(Instruction* i)
{
	CallSite cs(i);
	// skip intrinsic and declared functions without def.
	if (cs.getInstruction() && cs.getCalledFunction() && cs.getCalledFunction()->isIntrinsic()) {
		return true;
	}

	return false;
}

unordered_set<BasicBlock*>
LlvmPathAnal::split_blocks(Function &f)
{
	unordered_set<BasicBlock*> nbbs;
	SmallVector<pair<Instruction*, string>, 4> splits;
	unordered_set<Instruction*> sjs;

	DEBUG(errs() << "For function : " << f.getName() << "\n");
	auto i = 0;
	for (auto ii = inst_begin(f); ii != inst_end(f); ii++) {
		if (is_skip(&*ii)) {
			continue;
		}

		CallSite cs(&*ii);
		if (cs.getInstruction() && !ii->isTerminator()) {
			BasicBlock::iterator lbb(&*ii);
			auto ni = &*(++lbb);

			// Note: We can't the instances of setjmps, longjmps that uses func ptrs to them.
			Function* csf = cs.getCalledFunction();
			if (csf && csf->isDeclaration()) {
				if (csf->getName() == "_setjmp") {
					sjs.insert(ni);
				} else if (csf->getName() == "longjmp") {
					long_jmps.push_back(&*ii);
				} else {
					continue;
				}
			}

			DEBUG(errs() << "\tSplitting at :" << *ii << "\n");
			if (cs.getCalledFunction()) {
				splits.push_back(make_pair(ni, cs.getCalledFunction()->getName().str()));
			} else {
				splits.push_back(make_pair(ni, to_string(i++)));
			}
		}
	}

	for (auto &p : splits) {
		auto nbb = p.first->getParent()->splitBasicBlock(p.first, p.first->getParent()->getName() + "." + p.second + "_ret");
		if (sjs.find(p.first) != sjs.end()) {
			setjmp_rets.push_back(nbb->getFirstInsertionPt());
		}
		nbbs.insert(nbb);
	}

	return nbbs;
}

bool
LlvmPathAnal::doInitialization(Module &m)
{
	DEBUG(errs() << "In Path Analysis\n");
	auto &ctx = m.getContext();
	auto* int64Ty = Type::getInt64Ty(ctx);
	auto* int8Ty = Type::getInt8Ty(ctx);
	auto* voidTy = Type::getVoidTy(ctx);

	auto path_reg = m.getOrInsertGlobal(UNQ_NAME(path_reg), int64Ty);
	auto logsym_fn =  m.getOrInsertFunction(UNQ_NAME(log_sym), voidTy, int64Ty, nullptr);
	auto logpath_fn = m.getOrInsertFunction(UNQ_NAME(log_path), voidTy, nullptr);

	auto to_bbpos = [](BasicBlock* bb, InstrPos pos) {
		Instruction* p;
		if (pos == InstrPos::BEGIN) {
			p = bb->getFirstInsertionPt();
		} else if (pos == InstrPos::END) {
			p = bb->getTerminator();
		} else {
			assert(false && "InstrPos has unhandled Type");
		}

		return p;
	};

	symlogf = [int8Ty, int64Ty, logsym_fn](Instruction* i, u8 sym, u64 val) {
		Value *args[] = {
			ConstantInt::get(int64Ty, mk_trace_sym(val, sym), false)
		};
		CallInst::Create(logsym_fn, args, "", i);
	};

	auto incrf = [to_bbpos, int64Ty, path_reg](BasicBlock* bb, u64 val, InstrPos pos) {
		if (val == 0) {
			return;
		}
		auto lpos = to_bbpos(bb, pos);
		auto old = new LoadInst(path_reg, "", lpos);
		auto nu = BinaryOperator::Create(Instruction::Add, old, ConstantInt::get(int64Ty, val, false), "", lpos);
		new StoreInst(nu, path_reg, lpos);
	};

	auto logf = [to_bbpos, int64Ty, path_reg, logpath_fn, this](BasicBlock* bb, InstrPos pos) {
		auto lpos = to_bbpos(bb, pos);

		CallInst::Create(logpath_fn, "", lpos);
	};

	auto initf = [to_bbpos, int64Ty, path_reg](BasicBlock* bb, u64 val, InstrPos pos) {
		auto lpos = to_bbpos(bb, pos);
		new StoreInst(ConstantInt::get(int64Ty, val, false), path_reg, lpos);
	};

	auto realf = [this, incrf](BasicBlock* sbb, BasicBlock* tbb, u64 val) {
		auto nbb = SplitEdgeWrapper(sbb, tbb, this);
		incrf(nbb, val, InstrPos::BEGIN);
	};

	auto backf = [this, logf, initf, incrf](BasicBlock* sbb, BasicBlock* tbb, u64 in_val, u64 out_val) {
		auto nbb = SplitEdgeWrapper(sbb, tbb, this);
		logf(nbb, InstrPos::BEGIN);
		incrf(nbb, out_val, InstrPos::BEGIN);
		initf(nbb, in_val, InstrPos::END);
	};

	instr_tab = InstrTab<BasicBlock*>(incrf, logf, initf, realf, backf);

	return true;
}

void
LlvmPathAnal::Instrument(void)
{
	if(ppenc_graph == nullptr) {
		return;
	}

	// function entry.
	symlogf(ppenc_graph->Entry()->getFirstInsertionPt(), static_cast<u8>(TraceSym::E), fid);

	// setjmp rets.
	for (auto i : setjmp_rets) {
		DEBUG(errs() << "\tSetjmp ret Instr at : " <<  *i << "\n");
		symlogf(i, static_cast<u8>(TraceSym::S), fid);
	}

	// longjmp calls.
	for (auto i : long_jmps) {
		DEBUG(errs() << "\tLongjmp entry Instr at : " <<  *i << "\n");
		symlogf(i, static_cast<u8>(TraceSym::J), 0ULL);
	}

	// path Instrumentation.
	ppenc_graph->Instrument(instr_tab);

	auto pid = ppenc_graph->NumPaths();
	// self-loop instrumentation.
	for (auto &p : self_loops) {
		auto nbb = SplitEdgeWrapper(p.first, p.second, this);
		symlogf(nbb->getFirstInsertionPt(), static_cast<u8>(TraceSym::P), pid++);
	}

	// function exits. SelfNote: splitblocks are handled.
	for (auto& bb : ppenc_graph->Exits()) {
		auto t = bb->getTerminator();
		assert(t->getNumSuccessors() <= 1 &&
				"Atmost one successor for exit blocks due to splitting");
		if (t->getNumSuccessors() == 1) {
			symlogf(t->getSuccessor(0)->getTerminator(), static_cast<u8>(TraceSym::L), fid);
		} else {
			symlogf(t, static_cast<u8>(TraceSym::L), fid);
		}
	}
}

bool
LlvmPathAnal::runOnFunction(Function& f)
{
	if (f.empty()) {
		return true;
	}

	auto &LP = getAnalysis<LlvmPersist>();
	fid = LP.unique_fids[f.getName()];

	// Encoding of paths in the graph. This modifies the IR by splitting at Callsites.
	auto rets = split_blocks(f);

	ppenc_graph = std::make_unique<PathProf<BasicBlock*>>(&f.getEntryBlock());

	vector<pair<BasicBlock*, BasicBlock*>> fakelist;
	for (auto &bb : f) {
		// Note: No extra magic is required for indirectbr. We have the successor list.
		for(auto sbb = succ_begin(&bb); sbb != succ_end(&bb); sbb++) {
			if (rets.find(*sbb) == rets.end()) {
				if (&bb != *sbb) {
					ppenc_graph->CreateEdge(&bb, *sbb);
				} else {
					self_loops.push_back(make_pair(&bb, *sbb));
				}
			} else {	/* return from call. */
				fakelist.push_back(make_pair(&bb, *sbb));
			}
		}
	}
	
	// Process all backedges and the fakelist.
	ppenc_graph->Process(fakelist);

	// Compute the encoding of edges.
	ppenc_graph->ComputeVal();

	// In test mode, modify IR with path instrumentation.
	if (PathInst_test) {
		Instrument();
	}

#ifdef WITH_DEBUG
	ppenc_graph->Print([](BasicBlock* bb) { return bb->getName(); });
#endif

	return true;
}

void
LlvmPathAnal::releaseMemory(void)
{
	ppenc_graph.reset(nullptr);
	setjmp_rets.clear();
	long_jmps.clear();
	self_loops.clear();
}
}
