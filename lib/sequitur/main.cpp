#include <iostream>
#include <cstdint>
#include <vector>
#include <functional>
#include "sequitur.h"

using namespace sequitur;

int
main() {
	std::vector<int> s0 ({1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1});
	std::vector<int> s1 ({1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3});
	auto f = [](vector<int> v) {
		s32 next = 0;
		Sequitur<int> c(false, [&next](){return --next;});
		for (auto &i : v) {
			c.Process(i);
		}
		cout << "next set: \n";
	};
	f(s0);
	f(s1);
	return 0;
}

