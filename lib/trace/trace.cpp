#include "trace.h"

namespace trace {
size_t
TraceHeader::size()
{
	return prog.length() + 1 + 3*io::size<u8>::value;
}

Trace::Trace()
: path(""), hdr(), f()
{}

Trace::Trace(std::string path, TraceHeader hdr)
: path(path), hdr(hdr)
{
	f = std::move(io::MmapFile(path, true));
	assert(f.empty() && "File not empty");
	io::marshal<TraceHeader>(f.begin<u8>(), hdr);
}

Trace::Trace(std::string path)
: path(path)
{
	f = std::move(io::MmapFile(path));
	assert(!f.empty() && "File is empty");
	io::unmarshal<TraceHeader>(f.cbegin<u8>(), hdr);
}
}

