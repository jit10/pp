#include <string>
#include <system_error>

#include "dedup_trace.h"

namespace trace {
TraceSet::TraceSet(std::string prog, TraceType t)
: t(t), prog(prog)
{}

dedup::dedup_table_t<FilePtr>&
TraceSet::cons_table()
{
	return hcons_tbl;
}

// TODO: this needs fixing need to run LZ decompress.
void
TraceSet::load(std::string path)
{
	auto t = Trace(path);
	auto n = t.hdr.id;
	if (traces.size() <= n) {
		traces.resize(n+1);
	}
	traces[n] = std::move(t);
	io::MmapFile::const_iterator<u64> fb(traces[n].f, t.hdr.size());
	auto fe = traces[n].f.cend<u64>();
	std::function<FilePtr(decltype(fb))> it2ptr =
	[n, fb](decltype(fb) it) {
		return FilePtr(mk_trace_sym(mkFilePtr(n, it - fb), TraceSym::I));
	};
	dedup::dedup_parse<u64, decltype(fb), decltype(traces[n].f)>(hcons_tbl, fb, fe, it2ptr);
}

Trace&
TraceSet::create(std::string path)
{
	traces.push_back(Trace(path, TraceHeader(prog, DEDUP, t, traces.size())));
	return traces.back();
}
}

namespace dedup {
template<>
bool
is_start(const u64& s)
{
	return is_sym_of(s, TraceSym::E);
}

template<>
bool
is_end(const u64& s)
{
	return is_sym_of(s, TraceSym::L);
}

template<>
bool
is_ptr(const u64& s)
{
	return is_sym_of(s, TraceSym::L);
}

template<>
u64
mk_cpr_sym(size_t len, size_t off)
{
	// assert("len <= 65535 && off <= 65535 && "len or off too big");
	return mk_trace_sym((len << 16)^(off & 0xFFFFULL), TraceSym::C);
}

template<>
size_t
cpr_off(const u64& s)
{
	return (s & (0xFFFFULL));
}

template<>
size_t
cpr_len(const u64& s)
{
	return ((s >> 16) & 0xFFFFULL);
}
}

