if ("${CMAKE_SIZEOF_VOID_P}" EQUAL "4")
	message(FATAL_ERROR "32-bit programs are not supported")
endif ()

add_library(tracert SHARED
	runtime.cpp
)
