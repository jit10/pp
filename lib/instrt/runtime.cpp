#include "common.h"
#include "io.h"
#include "shadow_mem.h"

#include <vector>

#define EFIFO 0xef1f0
#define EMMAP 0xeadd5
#define ECSTK 0xe5710

#define STREAM_BUF_SZ 16*1024

extern "C" {
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

// path id.
u64 UNQ_VAR(path_reg);
// monotonic timestamp of path.
u64 UNQ_VAR(ts_reg);

// no. of vals per path.
u32 UNQ_VAR(nvals_reg);

//no. of data dependence timestamps.
u32 UNQ_VAR(nddeps_reg);

// current function frame addr.
u8* UNQ_VAR(cur_frame_addr);
//no. of ctrl dependence timestamps.
u32 UNQ_VAR(ncdeps_reg);

FILE *vfs, *dfs, *pfs, *cfs, *ctrlfs;

// Control dependency stack.(elem = BPID x TS, IBPID x FRAMEPTR)
std::vector<std::pair<std::pair<u32, u64>, std::pair<u32, u8*>>> cds;

void
UNQ_VAR(rt_init)(void)
{
	if (mkfifo(VAL_STREAM, 0600) && errno != EEXIST) {
		exit(EFIFO);
	}

	if (mkfifo(DDEP_STREAM, 0600) && errno != EEXIST) {
		exit(EFIFO);
	}

	if (mkfifo(CDEP_STREAM, 0600) && errno != EEXIST) {
		exit(EFIFO);
	}

	if (mkfifo(PATH_STREAM, 0600) && errno != EEXIST) {
		exit(EFIFO);
	}

	if (mkfifo(CTRL_STREAM, 0600) && errno != EEXIST) {
		exit(EFIFO);
	}

	if (((vfs = fopen(VAL_STREAM, "w")) == NULL)
		|| ((dfs = fopen(DDEP_STREAM, "w")) == NULL)
		|| ((cfs = fopen(CDEP_STREAM, "w")) == NULL)
		|| ((pfs = fopen(PATH_STREAM, "w")) == NULL)
		|| ((ctrlfs = fopen(CTRL_STREAM, "w")) == NULL)
	) {
		exit(EFIFO);
	}
	setvbuf(vfs, NULL, _IOFBF, STREAM_BUF_SZ);
	setvbuf(dfs, NULL, _IOFBF, STREAM_BUF_SZ);
	setvbuf(cfs, NULL, _IOFBF, STREAM_BUF_SZ);
	setvbuf(pfs, NULL, _IOFBF, STREAM_BUF_SZ);
	setvbuf(ctrlfs, NULL, _IOFBF, STREAM_BUF_SZ);
}

void
UNQ_VAR(rt_fini)(void)
{
	fclose(vfs);
	fclose(dfs);
	fclose(cfs);
	fclose(pfs);
	fclose(ctrlfs);
};

/* shadow memory init and cleanup */
void
UNQ_VAR(shadow_alloc)(void)
{
	int flags = MAP_ANONYMOUS | MAP_FIXED | MAP_PRIVATE | MAP_NORESERVE;
	if((u64)(mmap((void*)SHADOW_BEGIN, SHADOW_END - SHADOW_BEGIN,
		PROT_READ | PROT_WRITE, flags, -1, 0)) != SHADOW_BEGIN) {
		exit(EMMAP);
	}
}

void
UNQ_VAR(shadow_free)(void)
{
	if(munmap((void*)SHADOW_BEGIN, SHADOW_END - SHADOW_BEGIN)) {
		exit(EMMAP);
	}
}
/* */

void
UNQ_VAR(log_sym)(u64 sym)
{
	u8 buf[8];
	io::marshal<u64>(buf, sym);
	fwrite(&buf, 1, sizeof buf, pfs);
}

void
UNQ_VAR(log_path)(void)
{
	u8 buf[8];
	io::marshal<u64>(buf, mk_trace_sym(UNQ_VAR(path_reg), TraceSym::P));
	fwrite(&buf, 1, sizeof buf, pfs);

	// Increment timestamp.
	UNQ_VAR(ts_reg)++;

	// Control Information. At the end of each path we dump the ID:LEN onto ctrl stream.
	io::marshal<u32>(buf, mkctrl_sym(TraceType::VAL, UNQ_VAR(nvals_reg)));
	fwrite(&buf, 1, 4, ctrlfs);
	UNQ_VAR(nvals_reg) = 0;

	io::marshal<u32>(buf, mkctrl_sym(TraceType::DDEP, UNQ_VAR(nddeps_reg)));
	fwrite(&buf, 1, 4, ctrlfs);
	UNQ_VAR(nddeps_reg) = 0;

	io::marshal<u32>(buf, mkctrl_sym(TraceType::CDEP, UNQ_VAR(ncdeps_reg)));
	fwrite(&buf, 1, 4, ctrlfs);
	UNQ_VAR(ncdeps_reg) = 0;
}

/* Value logging runtime */
// N <= 64
#define LOG_VAL(N) \
void \
UNQ_VAR(log_val ## N)(u ## N val) \
{ \
	u8 buf[8]; \
	io::marshal<u ## N>(buf, val); \
	fwrite(&buf, 1, N/8, vfs); \
	UNQ_VAR(nvals_reg)++; \
}

LOG_VAL(8)
LOG_VAL(16)
LOG_VAL(32)
LOG_VAL(64)

void
UNQ_VAR(log_val)(u8* addr, u8 size)
{
	for (u8 i = 0; i < size; i++) {
		fwrite(addr++, 1, 1, vfs);
	}

	UNQ_VAR(nvals_reg)++;
}
/* */

/* data dependencies runtime */
void
UNQ_VAR(log_datadep)(u64 addr)
{
	u64 shadow_ts = *((u64*)MEM2SHADOW(addr));
	
	u8 buf[8];
	io::marshal<u64>(buf, shadow_ts);
	fwrite(&buf, 1, sizeof buf, dfs);

	UNQ_VAR(nddeps_reg)++;
}
/* */

/* control dependencies runtime */
void
UNQ_VAR(branching)(u32 bp_id, u32 ipd_id)
{
	auto e = std::make_pair(std::make_pair(bp_id, UNQ_VAR(ts_reg)), std::make_pair(ipd_id, UNQ_VAR(cur_frame_addr)));
	if (!cds.empty()) {
		auto &p = cds.back();
		if (p.second == std::make_pair(ipd_id, UNQ_VAR(cur_frame_addr))) {
			p.first.first = bp_id;
		} else {
			cds.push_back(e);
		}
	} else {
		cds.push_back(e);
	}
}

void
UNQ_VAR(merging)(u32 ipd_id)
{
	auto p = std::make_pair(ipd_id, UNQ_VAR(cur_frame_addr));
	if (cds.empty()) {
		exit(ECSTK);
	}
	auto e = cds.back();
	if (e.second == p) {
		cds.pop_back();
	}
}

void
UNQ_VAR(log_ctrldep)()
{
	u8 buf[12];
	u32 bbN = 0;
	u64 ts = 0;
	if (!cds.empty()) {
		auto p = cds.back().first;
		bbN = p.first;
		ts = p.second;
	}
	io::marshal<u32>(buf, bbN);
	io::marshal<u64>(buf+4, ts);
	fwrite(&buf, 1, sizeof buf, cfs);

	UNQ_VAR(ncdeps_reg)++;
}
/* */
}
